REPORT yddg_st03 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: metric_dummy TYPE yddg_metric.

PARAMETERS: p_res   TYPE swnctimeres DEFAULT 60.

SELECTION-SCREEN SKIP 1.

SELECT-OPTIONS s_metric FOR metric_dummy OBLIGATORY NO INTERVALS.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'ST03'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

  SELECT 'I'    AS sign,
         'EQ'   AS option,
         handle AS low
    FROM yddg_metrics
    WHERE handle LIKE 'ST03_%'
    INTO CORRESPONDING FIELDS OF TABLE @s_metric.

START-OF-SELECTION.

  NEW yddg_p_st03( metrics_range = s_metric[]
                   resolution    = p_res
                   tag_range     = s_tag[]
                   trace_active  = p_trace )->run( ).
