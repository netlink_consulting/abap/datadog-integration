CLASS yddg__event DEFINITION PUBLIC CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: handle   TYPE yddg_metric READ-ONLY,
          name     TYPE yddg_metric_name READ-ONLY,
          last_run TYPE yddg_ts READ-ONLY.

    METHODS:

      constructor IMPORTING !handle       TYPE yddg_metric
                            !tags         TYPE REF TO yddg__tags OPTIONAL
                            !trace_active TYPE abap_bool DEFAULT abap_false
                            !package_size TYPE i DEFAULT 100,

      append IMPORTING !aggregation_key  TYPE string OPTIONAL
                       !alert_type       TYPE string DEFAULT 'error'
                       !timestamp        TYPE yddg_ts
                       !device_name      TYPE string OPTIONAL
                       !priority         TYPE string OPTIONAL
                       !related_event_id TYPE int8 OPTIONAL
                       !text             TYPE string
                       !title            TYPE string
                       !tags             TYPE yddg_tag_tt OPTIONAL
             RETURNING VALUE(all_tags)   TYPE yddg_tag_tt,

      "! <p class="shorttext synchronized" lang="en">dequeue (unlock)</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en"></p>
      close IMPORTING      !timestamp TYPE yddg_ts,

      "! <p class="shorttext synchronized" lang="en">Send metric to Datadog</p>
      "!
      "! @parameter tries | <p class="shorttext synchronized" lang="en">Number of retries</p>
      send IMPORTING !tries TYPE i DEFAULT 3
           RAISING   yddg_x_event_not_submitted.

  PROTECTED SECTION.

    TYPES: BEGIN OF item_type,
             aggregation_key  TYPE string,
             alert_type       TYPE string,
             date_happened    TYPE yddg_ts,
             device_name      TYPE string,
             host             TYPE string,
             priority         TYPE string,
             related_event_id TYPE int8,
             source_type_name TYPE string,
             tags             TYPE yddg_tag_tt,
             text             TYPE string,
             title            TYPE string,
           END OF item_type.

    DATA: items     TYPE STANDARD TABLE OF item_type.

    DATA: metric_type      TYPE int1,
          tags             TYPE REF TO yddg__tags,
          trace_active     TYPE abap_bool,
          package_size     TYPE i,
          is_closed        TYPE abap_bool VALUE abap_false,
          metric           TYPE REF TO yddg__metric,
          source_type_name TYPE string.

    METHODS:

      "! <p class="shorttext synchronized" lang="en">Send package of datapoints</p>
      "!
      "! @parameter package | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tries | <p class="shorttext synchronized" lang="en"></p>
      send_package IMPORTING !package LIKE items
                             !tries   TYPE i
                   RAISING   yddg_x_event_not_submitted.

ENDCLASS.



CLASS yddg__event IMPLEMENTATION.

  METHOD constructor.

    me->handle       = handle.
    me->tags         = tags.
    me->trace_active = trace_active.
    me->package_size = package_size.
    me->metric       = NEW #( handle = handle ).
    me->source_type_name       = me->metric->name.
    me->last_run     = me->metric->last_run.

  ENDMETHOD.



  METHOD append.

    IF is_closed = abap_true.
      MESSAGE e009 WITH handle.
    ENDIF.

    APPEND VALUE #( aggregation_key  = aggregation_key
                    alert_type       = alert_type
                    date_happened    = timestamp
                    device_name      = device_name
                    host             = yddg=>hostname
                    priority         = priority
                    related_event_id = related_event_id
                    source_type_name = source_type_name
                    tags             = me->tags->union( tags )
                    text             = text
                    title            = title ) TO items.

  ENDMETHOD.


  METHOD close.

    IF is_closed = abap_false.

      IF timestamp IS NOT INITIAL.
        SELECT
          SINGLE *
          FROM   yddg_metrics
          WHERE  handle = @handle
          INTO   @DATA(wa).
        wa-timestamp = timestamp.
        MODIFY yddg_metrics FROM wa.
      ENDIF.

      CALL FUNCTION 'DEQUEUE_EYDDG_METRIC'
        EXPORTING
          handle = handle.

      is_closed = abap_true.

    ENDIF.

  ENDMETHOD.



  METHOD send.

    DATA: package LIKE items.

    WHILE lines( items ) > 0.
      CLEAR package.
      IF lines( items ) > package_size.
        APPEND LINES OF items FROM 1 TO package_size TO package.
        DELETE items FROM 1 TO package_size.
      ELSE.
        package = items.
        CLEAR items.
      ENDIF.
      send_package( package = package
                    tries   = tries ).
    ENDWHILE.

  ENDMETHOD.


  METHOD send_package.
    DATA: json        TYPE string,
          http_status TYPE i,
          response    TYPE string.

    LOOP AT package ASSIGNING FIELD-SYMBOL(<p>).

      json = /ui2/cl_json=>serialize( data = <p>
                                      compress = abap_true
                                      pretty_name = /ui2/cl_json=>pretty_mode-low_case ).
      DATA(client) = yddg=>api_client( 'v1/events' ).
      client->request->set_method( if_http_request=>co_request_method_post ).
      client->request->set_cdata( data = json ).
      client->send( timeout = if_http_client=>co_timeout_default ).
      CALL METHOD client->receive
        EXCEPTIONS
          http_communication_failure = 1                " Communication Error
          http_invalid_state         = 2                " Invalid state
          http_processing_failed     = 3                " Error When Processing Method
          OTHERS                     = 4.
      IF sy-subrc <> 0.
        http_status = 41800 + sy-subrc.
      ELSE.
        client->response->get_status( IMPORTING code = http_status ).
      ENDIF.
      IF http_status <> 202.
        response = client->response->get_cdata( ).
      ENDIF.
      client->close( ).
      DATA(rows) = lines( package ).

      IF http_status = 202.
        IF trace_active = abap_true.
          yddg=>write_json( json ).
        ENDIF.
        MESSAGE s010 WITH rows me->handle.
      ELSE.
        IF tries > 0.
          send_package( package = package
                        tries   = tries - 1 ).
        ELSE.
          SKIP 2.
          WRITE: / 'Cannot submit events', me->handle.
          WRITE: / 'HTTP_STATUS:', http_status.
          WRITE: / 'Response:', response.
          SKIP 1.
          yddg=>write_json( json ).
          RAISE EXCEPTION TYPE yddg_x_event_not_submitted
            EXPORTING
              count       = rows
              metric      = me->handle
              http_status = http_status.
        ENDIF.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
