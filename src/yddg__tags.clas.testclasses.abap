CLASS test_yddg__tags DEFINITION FOR TESTING
DURATION SHORT RISK LEVEL HARMLESS.

  PRIVATE SECTION.

    METHODS:
      test_contructor_empty FOR TESTING,
      test_contructor_tags  FOR TESTING,
      test_contructor_range FOR TESTING,
      test_add_empty        FOR TESTING,
      test_add_tag          FOR TESTING,
      test_add_tags         FOR TESTING,
      test_add_range        FOR TESTING,
      test_union_empty      FOR TESTING,
      test_union_tags       FOR TESTING,
      test_union_range      FOR TESTING.

ENDCLASS.



CLASS test_yddg__tags IMPLEMENTATION.

  METHOD test_contructor_empty.

    DATA(len) = lines( yddg=>tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( NEW yddg__tags( )->tags )  exp = len ).

  ENDMETHOD.


  METHOD test_contructor_range.

    DATA(range) = VALUE yddg_tag_range_tt( ( sign = 'I'  option = 'EQ'  low = 'ABC'  high = 'NOT' )
                                           ( sign = 'I'  option = 'EQ'  low = 'DEF' )
                                           ( sign = 'I'  option = 'EQ'  low = 'ABC' )
                                           ( sign = 'E'  option = 'EQ'  low = 'ZZZ' )
                                           ( sign = 'I'  option = 'CP'  low = 'DEF*' ) ).

    DATA(instance) = NEW yddg__tags( range = range ).

    DATA(len) = lines( yddg=>tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 2 ).

  ENDMETHOD.


  METHOD test_contructor_tags.

    DATA(tags) = VALUE yddg_tag_tt( ( 'ABC' )
                                    ( 'DEF' )
                                    ( 'ABC' )
                                    ( 'ZZZ' )
                                    ( 'DEF' ) ).

    DATA(instance) = NEW yddg__tags( tags ).

    DATA(len) = lines( yddg=>tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 3 ).

  ENDMETHOD.


  METHOD test_add_empty.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    instance->add( ).

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len ).

  ENDMETHOD.


  METHOD test_add_range.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    instance->add( range = VALUE yddg_tag_range_tt( ( sign = 'I'  option = 'EQ'  low = 'DEF' )
                                                    ( sign = 'I'  option = 'EQ'  low = 'ABC' ) ) ).

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 1 ).

  ENDMETHOD.


  METHOD test_add_tag.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    instance->add( 'DEF' ).

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 1 ).

    instance->add( 'DEF' ).

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 1 ).

  ENDMETHOD.


  METHOD test_add_tags.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    instance->add( tags = VALUE yddg_tag_tt( ( 'ABC' )
                                             ( 'DEF' ) ) ).

    cl_abap_unit_assert=>assert_equals( act = lines( instance->tags )  exp = len + 1 ).

  ENDMETHOD.


  METHOD test_union_empty.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( instance->union( ) )  exp = len ).

  ENDMETHOD.


  METHOD test_union_range.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( instance->union( range = VALUE yddg_tag_range_tt( ( sign = 'I'  option = 'EQ'  low = 'DEF' )
                                                                                                       ( sign = 'I'  option = 'EQ'  low = 'ABC' ) ) ) )  exp = len + 1 ).

  ENDMETHOD.


  METHOD test_union_tags.

    DATA(instance) = NEW yddg__tags( VALUE yddg_tag_tt( ( 'ABC' ) ) ).

    DATA(len) = lines( instance->tags ) .

    cl_abap_unit_assert=>assert_equals( act = lines( instance->union( VALUE yddg_tag_tt( ( 'ABC' )
                                                                                         ( 'DEF' ) ) ) )  exp = len + 1 ).

  ENDMETHOD.

ENDCLASS.
