CLASS yddg__p_scc4 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC.

  PUBLIC SECTION.

    TYPES:     client_range_type TYPE RANGE OF mandt .

    CLASS-METHODS: class_constructor.

    METHODS: constructor     IMPORTING !client_range       TYPE client_range_type OPTIONAL
                                       !client_specific    TYPE abap_bool DEFAULT abap_true
                                       !cross_client       TYPE abap_bool DEFAULT abap_true
                                       !copy_protection    TYPE abap_bool DEFAULT abap_true
                                       !catt_restriction   TYPE abap_bool DEFAULT abap_true
                                       !upgrade_protection TYPE abap_bool DEFAULT abap_true
                                       !tag_range          TYPE yddg_tag_range_tt OPTIONAL
                                       !trace_active       TYPE abap_bool OPTIONAL.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             mandt      TYPE mandt,
             cccategory TYPE cccategory,
             cccoractiv TYPE cccoractiv,
             ccnocliind TYPE ccnocliind,
             cccopylock TYPE cccopylock,
             ccimaildis TYPE ccimaildis,
             ccnocascad TYPE ccnocascad,
           END OF data_type,

           BEGIN OF result_type,
             property   TYPE c LENGTH 2,
             mandt      TYPE mandt,
             cccategory TYPE cccategory,
             value      TYPE typint8,
           END OF result_type,

           BEGIN OF client_role_type,
             domvalue_l TYPE rslgclasid,
             ddtext     TYPE val_text,
           END OF client_role_type,

           BEGIN OF metrics_type,
             property TYPE c LENGTH 2,
             metric   TYPE REF TO yddg__metric,
           END OF metrics_type.

    CONSTANTS: BEGIN OF map,
                 cs TYPE c LENGTH 4 VALUE '213.',
                 cc TYPE c LENGTH 4 VALUE '321.',
                 cp TYPE c LENGTH 3 VALUE 'LX.',
                 cr TYPE c LENGTH 5 VALUE '.FTEX',
                 up TYPE c LENGTH 2 VALUE 'X.',
               END OF map,

               BEGIN OF fld_idx,
                 cs TYPE i VALUE 3,
                 cc TYPE i VALUE 4,
                 cp TYPE i VALUE 5,
                 cr TYPE i VALUE 6,
                 up TYPE i VALUE 7,
               END OF fld_idx.

    CLASS-DATA: client_role TYPE SORTED TABLE OF client_role_type WITH UNIQUE KEY domvalue_l.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.


    " set in constructor
    DATA: client_range TYPE client_range_type,
          metrics      TYPE STANDARD TABLE OF metrics_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_scc4 IMPLEMENTATION.


  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'       val = row-mandt )                                         TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'CLIENT_ROLE'  val = client_role[ domvalue_l = row-cccategory ]-ddtext ) TO tags.  " sap_client_role

  ENDMETHOD.


  METHOD class_constructor.

    SELECT domvalue_l, ddtext
      FROM dd07t
      WHERE domname = 'CCCATEGORY'
        AND ddlanguage = 'E'
      INTO TABLE @client_role.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->client_range = client_range.

    IF client_specific    = abap_true.  APPEND VALUE #( property = 'CS'  metric = get_metric( handle = 'SCC4_CS'  tags = tags  trace_active = trace_active ) ) TO metrics.  ENDIF.
    IF cross_client       = abap_true.  APPEND VALUE #( property = 'CC'  metric = get_metric( handle = 'SCC4_CC'  tags = tags  trace_active = trace_active ) ) TO metrics.  ENDIF.
    IF copy_protection    = abap_true.  APPEND VALUE #( property = 'CP'  metric = get_metric( handle = 'SCC4_CP'  tags = tags  trace_active = trace_active ) ) TO metrics.  ENDIF.
    IF catt_restriction   = abap_true.  APPEND VALUE #( property = 'CR'  metric = get_metric( handle = 'SCC4_CR'  tags = tags  trace_active = trace_active ) ) TO metrics.  ENDIF.
    IF upgrade_protection = abap_true.  APPEND VALUE #( property = 'UP'  metric = get_metric( handle = 'SCC4_UP'  tags = tags  trace_active = trace_active ) ) TO metrics.  ENDIF.

  ENDMETHOD.


  METHOD extract.

    SELECT mandt,
           cccategory,
           cccoractiv,
           ccnocliind,
           cccopylock,
           ccimaildis,
           ccnocascad
       FROM t000
       WHERE mandt IN @client_range
       INTO TABLE @data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    FIELD-SYMBOLS: <map> TYPE any,
                   <idx> TYPE i,
                   <fld> TYPE c.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).
        MOVE-CORRESPONDING <d> TO result_row.
        result_row-property = <m>-property.
        ASSIGN COMPONENT <m>-property OF STRUCTURE map TO <map>.
        ASSIGN COMPONENT <m>-property OF STRUCTURE fld_idx TO <idx>.
        ASSIGN COMPONENT <idx> OF STRUCTURE <d> TO <fld>.
        result_row-value = find( val = <map>  sub = SWITCH char01( <fld> WHEN space THEN '.' ELSE <fld> ) ) + 1.
        APPEND result_row TO result_table.
      ENDLOOP.
    ENDLOOP.

    DELETE result_table WHERE property = 'UP' AND cccategory <> 'T'.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>) WHERE property = <m>-property.
        <m>-metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).
      ENDLOOP.

      TRY.
          <m>-metric->send( ).
        CATCH yddg_x_metric_not_submitted INTO DATA(cx).
          MESSAGE cx TYPE yddg=>submit_error_severity.
      ENDTRY.

    ENDLOOP.

  ENDMETHOD.


  METHOD finalize.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      <m>-metric->close( timestamp ).

    ENDLOOP.

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
