CLASS yddg__p_sm58 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: user_range_type            TYPE RANGE OF xubname,
           function_module_range_type TYPE RANGE OF rs38l_fnam,
           dest_range_type            TYPE RANGE OF rfcdest,
           status_range_type          TYPE RANGE OF arfcstate.

    METHODS: constructor IMPORTING !user_range            TYPE user_range_type OPTIONAL
                                   !function_module_range TYPE function_module_range_type OPTIONAL
                                   !dest_range            TYPE dest_range_type OPTIONAL
                                   !status_range          TYPE status_range_type OPTIONAL
                                   !bracket_set           TYPE yddg_bracket DEFAULT 'AGE'
                                   !other_tag             TYPE yddg_tag DEFAULT 'sap_age:other'
                                   !tag_range             TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active          TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             arfcdest  TYPE rfcdest,
             arfcstate TYPE arfcstate,
             arfcfnam  TYPE rs38l_fnam,
             arfcuzeit TYPE syuzeit,
             arfcdatum TYPE sydatum,
             arfcuser  TYPE syuname,
             arfctcode TYPE sytcode,
           END OF data_type,

           result_type TYPE yddg_sm58_rb,

           BEGIN OF selector_type,
             user_range            TYPE user_range_type,
             function_module_range TYPE function_module_range_type,
             dest_range            TYPE dest_range_type,
             status_range          TYPE status_range_type,
           END OF selector_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.


    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric       TYPE REF TO yddg__metric,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_sm58 IMPLEMENTATION.

  METHOD build_tags.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'DESTINATION'     val = row-arfcdest  ) TO tags.  " sap_destination
    APPEND yddg__tags=>resolve( handle = 'ARFC_STATUS'     val = row-arfcstate ) TO tags.  " sap_arfc_status
    APPEND yddg__tags=>resolve( handle = 'FUNCTION_MODULE' val = row-arfcfnam  ) TO tags.  " sap_function_module
    APPEND yddg__tags=>resolve( handle = 'USER_ID'         val = row-arfcuser  ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'TCODE'           val = row-arfctcode ) TO tags.  " sap_tcode

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

    metric = get_metric( handle = 'SM58'  tags  = tags  trace_active = trace_active ).

    me->selector-user_range            = user_range.
    me->selector-function_module_range = function_module_range.
    me->selector-dest_range            = dest_range.
    me->selector-status_range          = status_range.

  ENDMETHOD.


  METHOD extract.

    SELECT arfcdest,
           arfcstate,
           arfcfnam,
           arfcuzeit,
           arfcdatum,
           arfcuser,
           arfctcode
      FROM arfcsstate
      WHERE arfcuser   IN @selector-user_range
        AND arfcdest   IN @selector-dest_range
        AND arfcfnam   IN @selector-function_module_range
        AND arfcstate  IN @selector-status_range
        AND arfcreturn = @space
      INTO TABLE @data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_system = VALUE #( date    = <d>-arfcdatum
                                                                                    time    = <d>-arfcuzeit ) ) ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          tag       = <r>-tag
          arfcdest  = <r>-arfcdest
          arfcstate = <r>-arfcstate
          arfcfnam  = <r>-arfcfnam
          arfcuser  = <r>-arfcuser
          arfctcode = <r>-arfctcode
          value     = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag       = row->tag
          arfcdest  = row->arfcdest
          arfcstate = row->arfcstate
          arfcfnam  = row->arfcfnam
          arfcuser  = row->arfcuser
          arfctcode = row->arfctcode
          value     = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp LIKE result_table.

    SELECT tag,
           arfcdest,
           arfcstate,
           arfcfnam,
           arfcuser,
           arfctcode
      FROM yddg_sm58_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag   = <r>-tag
          arfcdest  = <r>-arfcdest
          arfcstate = <r>-arfcstate
          arfcfnam  = <r>-arfcfnam
          arfcuser  = <r>-arfcuser
          arfctcode = <r>-arfctcode ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_sm58_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_sm58_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
