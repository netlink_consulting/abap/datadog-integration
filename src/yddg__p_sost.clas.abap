CLASS yddg__p_sost DEFINITION
  PUBLIC
  INHERITING FROM yddg__p
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: user_range_type      TYPE RANGE OF xubname,
           send_type_range_type TYPE RANGE OF sx_addrtyp.

    METHODS: constructor IMPORTING
                           !user_range          TYPE user_range_type OPTIONAL
                           !send_type_range     TYPE send_type_range_type OPTIONAL
                           !max_age_current     TYPE yddg_seconds
                           !bracket_set_current TYPE yddg_bracket DEFAULT 'AGE'
                           !other_tag_current   TYPE yddg_tag DEFAULT 'sap_age:other'
                           !max_age_error       TYPE yddg_seconds
                           !bracket_set_error   TYPE yddg_bracket DEFAULT 'AGE'
                           !other_tag_error     TYPE yddg_tag DEFAULT 'sap_age:other'
                           !tag_range           TYPE yddg_tag_range_tt OPTIONAL
                           !trace_active        TYPE abap_bool OPTIONAL.

  PROTECTED SECTION.

    TYPES:

      BEGIN OF metrics_type,
        handle      TYPE c LENGTH 1,
        metric      TYPE REF TO yddg__metric,
        bracket_set TYPE yddg_bracket,
        other_tag   TYPE yddg_tag,
        soststatus  TYPE soststatus,
        system_dt   TYPE yddg_datetime,
      END OF metrics_type,

      BEGIN OF selector_type,
        users      TYPE sxsenderrngt,
        send_types TYPE TABLE OF sx_addrtyp WITH DEFAULT KEY,
      END OF selector_type.

    " set in constructor
    DATA: metrics    TYPE STANDARD TABLE OF metrics_type,
          selector   TYPE selector_type,

          " set in extract
          data_table TYPE soxsp2tab.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

  PRIVATE SECTION.
ENDCLASS.



CLASS yddg__p_sost IMPLEMENTATION.

  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->selector-users = user_range.

    " send type specified, must be resolved, FM only takes single value

    IF send_type_range IS NOT INITIAL.
      SELECT domvalue_l
        FROM dd07l
        WHERE domvalue_l IN @send_type_range
        INTO TABLE @selector-send_types.
    ELSE.
      selector-send_types = VALUE #( ( space ) ).
    ENDIF.

    " if the age parameter for a metic is below 1, the metric is ignored

    IF max_age_current > 0.
      APPEND VALUE #( handle = 'C'
                      metric      = get_metric( handle = 'SOST_CURRENT'  tags = tags  trace_active = trace_active )
                      bracket_set = bracket_set_current
                      other_tag   = other_tag_current
                      soststatus  = VALUE #( wait    = abap_true
                                             transit = abap_true
                                             ok      = abap_true
                                             error   = abap_true
                                             incons  = abap_true
                                             future  = abap_true
                                             retry   = abap_true
                                             direct  = abap_true
                                             active  = abap_true )
                      system_dt   = yddg=>system( timestamp - max_age_current ) ) TO metrics.
    ENDIF.

    IF max_age_error > 0.
      APPEND VALUE #( handle = 'E'
                      metric      = get_metric( handle = 'SOST_ERROR'  tags = tags  trace_active = trace_active )
                      bracket_set = bracket_set_error
                      other_tag   = other_tag_error
                      soststatus  = VALUE #( error   = abap_true )
                      system_dt   = yddg=>system( timestamp - max_age_error ) ) TO metrics.
    ENDIF.

  ENDMETHOD.


  METHOD extract.

    DATA: temp LIKE data_table.

    " need to run FM twice for each metric because of messed up date / time selection except if date is same as today

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      " for each send type

      LOOP AT selector-send_types ASSIGNING FIELD-SYMBOL(<snd_art>).
        " run for the day in selector, using time

        CALL FUNCTION 'SX_SNDREC_SELECT'
          EXPORTING
            snd_art  = <snd_art>
            snd_date = VALUE sxdatrngt( ( sign = 'I' option = 'EQ' low = <m>-system_dt-date ) )
            snd_time = VALUE sxtimrngt( ( sign = 'I' option = 'GE' low = <m>-system_dt-time ) )
            status   = <m>-soststatus
            sender   = selector-users
            maxsel   = 2147483647
          IMPORTING
            sndrecs  = temp.

        APPEND LINES OF temp TO data_table.

        " if the date is not today, also get records on > date only

        IF <m>-system_dt-date <> sy-datum.

          CALL FUNCTION 'SX_SNDREC_SELECT'
            EXPORTING
              snd_art  = <snd_art>
              snd_date = VALUE sxdatrngt( ( sign = 'I' option = 'GT' low = <m>-system_dt-date ) )
              status   = <m>-soststatus
              sender   = selector-users
              maxsel   = 2147483647
            IMPORTING
              sndrecs  = temp.

          APPEND LINES OF temp TO data_table.

        ENDIF.

      ENDLOOP.

    ENDLOOP.

    SORT data_table.
    DELETE ADJACENT DUPLICATES FROM data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    " most of the code is adapted from SAPLSBCS_OUT

    TYPES: BEGIN OF address_t,
             adrnr      TYPE adcp-so_key,
             addrnumber TYPE adcp-addrnumber,
             persnumber TYPE adcp-persnumber,
             name_text  TYPE adrp-name_text,
             addr_type  TYPE szad_field-addr_type,
             error_flag TYPE szad_field-flag,
             adrtp      TYPE sx_addr_type,
           END OF address_t.

    DATA: bcs_sndrec TYPE REF TO cl_sndrec_bcs,
          bcs_status TYPE bcs_status,
          is_waiting TYPE abap_bool.

    DATA: sender_table TYPE HASHED TABLE OF soud3 WITH UNIQUE KEY usrtp usryr usrno,
          sender_row   LIKE LINE OF sender_table.

    DATA: recipient_table TYPE HASHED TABLE OF address_t WITH UNIQUE KEY adrtp adrnr,
          recipient_row   LIKE LINE OF recipient_table.


    CREATE OBJECT bcs_sndrec.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).

      IF <d>-status CN '0123456789 '.
        <d>-msgid  = 'SO'.
        <d>-msgno  = '672'.
        <d>-msgv1  = TEXT-t49.
        <d>-sost_msgid = 'SO'.
        <d>-sost_msgv1 = TEXT-t49.
      ELSE.
        <d>-msgno   = <d>-status.
      ENDIF.

      IF  <d>-msgno  =  73  OR  <d>-msgno  = 701  OR  <d>-msgno  = 702  OR  <d>-msgno  = 709  OR
          <d>-msgno  = 716  OR  <d>-msgno  = 717  OR  <d>-msgno  = 723  OR
          ( <d>-msgno >= 801 AND <d>-msgno <= 899 )  OR  ( <d>-msgno >= 602 AND <d>-msgno <= 698 ).
        <d>-def_stat = abap_true.
      ELSE.
        <d>-def_stat = abap_false.
      ENDIF.

      <d>-msgty = 'I'.
      bcs_sndrec->status_to_type(
        EXPORTING
          iv_status_code = <d>-status
          is_soesk       = VALUE soesk( rectp = <d>-rectp  recyr = <d>-recyr  recno = <d>-recno )
          iv_sndreq      = <d>-sndreq
        IMPORTING
          ev_status_type = bcs_status
          ev_is_waiting  = is_waiting
          ev_wait_date   = <d>-wait_date
          ev_wait_time   = <d>-wait_time ).

      IF is_waiting = abap_true.
        <d>-msgty = 'Q'.
      ENDIF.

      CASE bcs_status.
        WHEN cl_bcs=>gc_direct.
          <d>-msgv1 = 'Waiting to be sent directly (without send job)'.
          <d>-msgv2 = 'SOST_STATUS_DIRECT'.
        WHEN cl_bcs=>gc_retry.
          <d>-msgv2 = 'SOST_STATUS_RETRY'.
        WHEN cl_bcs=>gc_future.
          <d>-msgv1 = 'Will be sent after &1 &2'.
          <d>-msgv2 = 'SOST_STATUS_FUTURE'.
          REPLACE '&1' IN <d>-msgv1 WITH <d>-wait_date.
          REPLACE '&2' IN <d>-msgv1 WITH <d>-wait_time.
        WHEN cl_bcs=>gc_wait.
          <d>-msgv2 = 'SOST_STATUS_WAIT'.
        WHEN cl_bcs=>gc_incons.
          <d>-msgty = 'X'.
          <d>-msgv1 = 'Still no entry in queue'.
          <d>-msgv2 = 'SOST_STATUS_INCONS'.
        WHEN cl_bcs=>gc_ok.
        WHEN cl_bcs=>gc_error.
          <d>-msgty = 'E'.
        WHEN cl_bcs=>gc_err_intern.
          <d>-msgty = 'E'.
        WHEN cl_bcs=>gc_err_connect.
          <d>-msgty = 'E'.
        WHEN cl_bcs=>gc_err_extern.
          <d>-msgty = 'E'.
        WHEN OTHERS.
      ENDCASE.

      IF <d>-msgtp NE 'S'.

        IF NOT <d>-fortp IS INITIAL.
          sender_row-usrtp = <d>-fortp.
          sender_row-usryr = <d>-foryr.
          sender_row-usrno = <d>-forno.
        ELSE.                                                    "
          sender_row-usrtp = <d>-sndtp.
          sender_row-usryr = <d>-sndyr.
          sender_row-usrno = <d>-sndno.
        ENDIF.
        IF NOT line_exists( sender_table[ usrtp = sender_row-usrtp
                                          usryr = sender_row-usryr
                                          usrno = sender_row-usrno ] ).
          INSERT sender_row INTO TABLE sender_table.
        ENDIF.

        IF NOT line_exists( recipient_table[ adrtp = <d>-sndart
                                             adrnr = <d>-adrnr ] ).
          INSERT VALUE #( adrtp = <d>-sndart
                          adrnr = <d>-adrnr ) INTO TABLE recipient_table.
        ENDIF.
      ENDIF.

      IF <d>-msgtp IS INITIAL.
        <d>-msgtp = 'M'.
      ENDIF.

    ENDLOOP.

    write_table( name = 'data_table'  tab = data_table ).

    " resolve recipients

    DATA: recipients_resolve_table TYPE STANDARD TABLE OF address_t.

    LOOP AT recipient_table ASSIGNING FIELD-SYMBOL(<recipient>) WHERE name_text IS INITIAL.
      INSERT <recipient> INTO TABLE recipients_resolve_table.
    ENDLOOP.

    IF recipients_resolve_table IS NOT INITIAL.
      SELECT    f~so_key AS adrnr,
                f~addrnumber,
                f~persnumber,
                g~name_text
        FROM    adcp AS f
          JOIN  adrp AS g
            ON  f~persnumber = g~persnumber
            AND f~date_from  = g~date_from
            AND f~nation     = g~nation
        FOR ALL ENTRIES IN @recipients_resolve_table
        WHERE f~so_key = @recipients_resolve_table-adrnr
        INTO TABLE @DATA(recipients_temp_table).

      IF sy-subrc = 0.
        SORT recipients_temp_table BY addrnumber.
        LOOP AT recipients_resolve_table ASSIGNING <recipient>.
          CLEAR: recipient_row.
          READ TABLE recipients_temp_table INTO recipient_row WITH KEY adrnr = <recipient>-adrnr BINARY SEARCH.
          IF sy-subrc = 0 AND NOT recipient_row-name_text IS INITIAL.
            recipient_row-adrtp = <recipient>-adrtp.
            MODIFY TABLE recipient_table FROM recipient_row.
            DELETE TABLE recipients_resolve_table FROM recipient_row.
          ENDIF.
        ENDLOOP.
      ENDIF.
    ENDIF.

    IF recipients_resolve_table IS NOT INITIAL.

      PERFORM get_zav_names IN PROGRAM saplsbcs_out CHANGING recipients_resolve_table.
      LOOP AT recipients_resolve_table ASSIGNING <recipient>.
        MODIFY TABLE recipient_table FROM <recipient>.
      ENDLOOP.
    ENDIF.

    " resolve sender

    LOOP AT sender_table ASSIGNING FIELD-SYMBOL(<sender>) WHERE adrname IS INITIAL.
      CALL FUNCTION 'SO_NAME_CONVERT'
        EXPORTING
          deleted_included      = 'X'
          name_in               = <sender>
        IMPORTING
          name_out              = sender_row
        EXCEPTIONS
          communication_failure = 1
          office_name_not_exist = 2
          parameter_error       = 3
          sap_name_not_exist    = 2
          system_failure        = 5
          user_not_exist        = 2
          OTHERS                = 7.
      MODIFY TABLE sender_table FROM sender_row.
    ENDLOOP.

    LOOP AT data_table ASSIGNING <d> WHERE msgtp NE 'S'..

      IF NOT <d>-fortp IS INITIAL.
        sender_row-usrtp = <d>-fortp.
        sender_row-usryr = <d>-foryr.
        sender_row-usrno = <d>-forno.
      ELSE.                                                    "
        sender_row-usrtp = <d>-sndtp.
        sender_row-usryr = <d>-sndyr.
        sender_row-usrno = <d>-sndno.
      ENDIF.

      READ TABLE sender_table INTO sender_row
         WITH KEY usrtp = sender_row-usrtp
                  usryr = sender_row-usryr
                  usrno = sender_row-usrno.

      IF NOT sender_row-adrname IS INITIAL.
        <d>-sender = sender_row-adrname.
      ENDIF.

      IF NOT <d>-fortp IS INITIAL.
        IF NOT sender_row-sapnam IS INITIAL.
          <d>-usernam = sender_row-sapnam.
        ENDIF.
      ENDIF.

      <d>-address = recipient_table[ adrnr = <d>-adrnr  adrtp = <d>-sndart ]-name_text.

    ENDLOOP.

    write_table( name = 'data_table'  tab = data_table ).


  ENDMETHOD.


  METHOD load.

  ENDMETHOD.


  METHOD finalize.

  ENDMETHOD.

ENDCLASS.



