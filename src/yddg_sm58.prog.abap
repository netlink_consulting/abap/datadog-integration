REPORT yddg_sm58 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: user_dummy   TYPE xubname,
      fm_dummy     TYPE rs38l_fnam,
      dest_dummy   TYPE rfcdest,
      status_dummy TYPE arfcstate.

SELECT-OPTIONS: s_user   FOR user_dummy,
                s_fm     FOR fm_dummy,
                s_dest   FOR dest_dummy,
                s_status FOR status_dummy.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_brack TYPE yddg_bracket DEFAULT 'AGE',
            p_other TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_age:other' LOWER CASE.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'SM58'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_sm58( user_range            = s_user[]
                   function_module_range = s_fm[]
                   dest_range            = s_dest[]
                   status_range          = s_status[]
                   bracket_set           = p_brack
                   other_tag             = p_other
                   tag_range             = s_tag[]
                   trace_active          = p_trace )->run( ).
