REPORT yddg_smqs MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: mandt_dummy TYPE mandt.

SELECT-OPTIONS: s_mandt FOR mandt_dummy.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'SMQS'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_smqs( client_range = s_mandt[]
                   tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
