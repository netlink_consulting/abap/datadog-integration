CLASS yddg__brackets DEFINITION PUBLIC FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    METHODS:

      "! <p class="shorttext synchronized" lang="en">Read definition from table</p>
      "!
      "! @parameter bracket_set | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">not required for duration</p>
      "! @parameter other_tag | <p class="shorttext synchronized" lang="en"></p>
      constructor IMPORTING !bracket_set TYPE yddg_bracket
                            !timestamp   TYPE yddg_ts DEFAULT 0
                            !other_tag   TYPE yddg_tag DEFAULT 'sap_age:other',

      "! <p class="shorttext synchronized" lang="en">Return Bracket for duration</p>
      "!
      "! @parameter secs | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tag | <p class="shorttext synchronized" lang="en"></p>
      secs IMPORTING !secs      TYPE yddg_seconds
           RETURNING VALUE(tag) TYPE yddg_tag,

      "! <p class="shorttext synchronized" lang="en">Return Bracket for age</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tag | <p class="shorttext synchronized" lang="en"></p>
      delta IMPORTING !timestamp TYPE yddg_ts
            RETURNING VALUE(tag) TYPE yddg_tag.

  PROTECTED SECTION.

    DATA: brackets  TYPE HASHED TABLE OF yddg_bracket_thr WITH UNIQUE KEY secs,
          timestamp TYPE yddg_ts,
          other_tag TYPE yddg_tag.

ENDCLASS.



CLASS yddg__brackets IMPLEMENTATION.

  METHOD constructor.

    SELECT  tag,
            secs
      FROM  yddg_bracket_thr
      WHERE handle = @bracket_set
      INTO  CORRESPONDING FIELDS OF TABLE @brackets.

    me->timestamp = timestamp.
    me->other_tag = other_tag.

  ENDMETHOD.


  METHOD secs.

    tag = other_tag.

    LOOP AT brackets ASSIGNING FIELD-SYMBOL(<bracket>).
      IF secs <= <bracket>-secs.
        tag = <bracket>-tag.
        EXIT.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.


  METHOD delta.

    tag = secs( secs = me->timestamp - timestamp ).

  ENDMETHOD.

ENDCLASS.
