REPORT yddg_st22 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

data: mandt_dummy  TYPE mandt,
      user_dummy   TYPE xubname,
      ashost_dummy type SNAP_SYINST.

SELECT-OPTIONS: s_mandt FOR mandt_dummy,
                s_user FOR user_dummy MATCHCODE OBJECT user_comp,
                s_ashost FOR ashost_dummy.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  IF cl_st22_tools=>check_user_authorization_st22( ) <> 'X'.
    MESSAGE e003 WITH sy-repid.
  ENDIF.



START-OF-SELECTION.

  NEW yddg_p_st22( client_range = s_mandt[]
                   user_range   = s_user[]
                   ashost_range = s_ashost[]
                   tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
