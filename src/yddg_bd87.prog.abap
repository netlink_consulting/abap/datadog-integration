REPORT yddg_bd87 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: client_dummy       TYPE mandt,
      status_dummy       TYPE edi_status,
      partner_dummy      TYPE sendsystem,
      message_type_dummy TYPE edi_mestyp.


SELECT-OPTIONS: s_client FOR client_dummy.
PARAMETERS: p_out TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_in  TYPE char01 AS CHECKBOX DEFAULT abap_true.
SELECT-OPTIONS: s_status FOR status_dummy,
                s_partnr FOR partner_dummy,
                s_msgtyp FOR message_type_dummy.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_brack TYPE yddg_bracket DEFAULT 'AGE',
            p_other TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_age:other' LOWER CASE.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'BD87'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_bd87( client_range       = s_client[]
                   outbound           = p_out
                   inbound            = p_in
                   status_range       = s_status[]
                   partner_range      = s_partnr[]
                   message_type_range = s_msgtyp[]
                   bracket_set        = p_brack
                   other_tag          = p_other
                   tag_range          = s_tag[]
                   trace_active       = p_trace )->run( ).
