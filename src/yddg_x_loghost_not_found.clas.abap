CLASS yddg_x_loghost_not_found DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    METHODS: constructor IMPORTING !previous LIKE previous OPTIONAL.

ENDCLASS.



CLASS YDDG_X_LOGHOST_NOT_FOUND IMPLEMENTATION.

  METHOD constructor  ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '904' ).

  ENDMETHOD.

ENDCLASS.
