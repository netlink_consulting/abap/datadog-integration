FUNCTION CONVERSION_EXIT_YDDGL_OUTPUT.
*"--------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(INPUT)
*"  EXPORTING
*"     VALUE(OUTPUT)
*"--------------------------------------------------------------------

output = to_lower( input ).

ENDFUNCTION.
