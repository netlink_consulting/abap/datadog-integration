CLASS yddg__p_smqs DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt.

    METHODS: constructor IMPORTING !client_range TYPE client_range_type OPTIONAL
                                   !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             mandt   TYPE symandt,
             dest    TYPE rfcdest,
             maxconn TYPE dest_maxconn,
             nactive TYPE retry,
           END OF data_type,

           result_type TYPE data_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.


    " set in constructor
    DATA: client_range   TYPE client_range_type,
          metric_current TYPE REF TO yddg__metric,
          metric_max     TYPE REF TO yddg__metric,

          " set in extract
          data_table     TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table   TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_smqs IMPLEMENTATION.

  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'       val = row-mandt ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'DESTINATION'  val = row-dest  ) TO tags.  " sap_destination

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->client_range = client_range.

    metric_current = get_metric( handle = 'SMQS_CURRENT'  tags  = tags  trace_active = trace_active ).
    metric_max     = get_metric( handle = 'SMQS_MAX'      tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD extract.

    SELECT mandt,
           dest,
           maxconn,
           nactive
      FROM qsenddest CLIENT SPECIFIED
      WHERE mandt IN @client_range
        AND dest NOT LIKE '%%QSEND%'
      INTO TABLE @data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    result_table = data_table.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>).
      DATA(tags) = build_tags( <r> ).
      metric_current->append( timestamp = timestamp  value = CONV #( <r>-nactive ) tags = tags ).
      metric_max->append(     timestamp = timestamp  value = CONV #( <r>-maxconn ) tags = tags ).
    ENDLOOP.

    TRY.
        metric_current->send( ).
        metric_max->send( ).
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric_current->close( timestamp ).
    metric_max->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
