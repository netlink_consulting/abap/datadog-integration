REPORT yddg_init  MESSAGE-ID yddg.

PARAMETERS: p_apikey TYPE yddg_apikey OBLIGATORY,
            p_apihst TYPE yddg_cs_value VISIBLE LENGTH 50,
            p_loghst TYPE yddg_cs_value VISIBLE LENGTH 50,
            p_env    TYPE yddg_cs_value VISIBLE LENGTH 15,
            p_host   TYPE yddg_cs_value VISIBLE LENGTH 50.

INITIALIZATION.

  DATA(config_data) = yddg=>configuration( ).
  p_apikey = config_data-api_key.
  p_apihst = config_data-api_host.
  p_loghst = config_data-log_host.
  p_env    = config_data-env.
  p_host   = config_data-hostname.

START-OF-SELECTION.

  DATA: rows TYPE HASHED TABLE OF yddg_config WITH UNIQUE KEY handle.

  rows = VALUE #( ( handle = 'API_KEY'   value = p_apikey )
                  ( handle = 'API_HOST'  value = p_apihst )
                  ( handle = 'ENV'       value = p_env    )
                  ( handle = 'HOSTNAME'  value = p_host   )
                  ( handle = 'LOG_HOST'  value = p_loghst ) ).

  MODIFY yddg_config FROM TABLE rows.

  MESSAGE s000 WITH 'Updated Configuration.'.

  LEAVE PROGRAM.
