REPORT yddg_sm37 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: job_name_dummy TYPE btcjob,
      user_dummy     TYPE xubname.

SELECT-OPTIONS: s_job FOR job_name_dummy,
                s_user FOR user_dummy MATCHCODE OBJECT user_comp.

SELECTION-SCREEN SKIP 1.

SELECTION-SCREEN BEGIN OF BLOCK bl1 WITH FRAME TITLE TEXT-001.
PARAMETERS: p_run    TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_finish TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_abort  TYPE char01 AS CHECKBOX DEFAULT abap_true.
SELECTION-SCREEN END OF BLOCK bl1.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_brack TYPE yddg_bracket DEFAULT 'DURATION',
            p_other TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_duration:other' LOWER CASE.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
   ID 'TCD' FIELD 'SM37'.
  IF sy-subrc <> 0.
    MESSAGE e011 WITH sy-repid.
  ENDIF.

  AUTHORITY-CHECK OBJECT 'S_BTCH_ADM'
   ID 'BTCADMIN' FIELD 'Y'.
  IF sy-subrc <> 0.
    MESSAGE e011 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  IF p_trace = abap_true.
    WRITE: / 'Trace active.'.
  ENDIF.

  NEW yddg_p_sm37( job_name_range  = s_job[]
                   user_range      = s_user[]
                   status_running  = p_run
                   status_finished = p_finish
                   status_aborted  = p_abort
                   bracket_set     = p_brack
                   other_tag       = p_other
                   tag_range       = s_tag[]
                   trace_active    = p_trace )->run( ).
