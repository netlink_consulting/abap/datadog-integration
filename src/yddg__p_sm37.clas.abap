CLASS yddg__p_sm37 DEFINITION
  PUBLIC
  INHERITING FROM yddg__p
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: job_name_range_type TYPE RANGE OF btcjob,
           user_range_type     TYPE RANGE OF xubname.


    METHODS: constructor IMPORTING !job_name_range  TYPE job_name_range_type OPTIONAL
                                   !user_range      TYPE user_range_type OPTIONAL
                                   !status_running  TYPE abap_bool
                                   !status_finished TYPE abap_bool
                                   !status_aborted  TYPE abap_bool
                                   !bracket_set     TYPE yddg_bracket DEFAULT 'DURATION'
                                   !other_tag       TYPE yddg_tag DEFAULT 'sap_duration:other'
                                   !tag_range       TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active    TYPE abap_bool OPTIONAL.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             jobname    TYPE btcjob,
             lastchname TYPE btcjchnm,
             strtdate   TYPE btcxdate,
             strttime   TYPE btcxtime,
             enddate    TYPE btcxdate,
             endtime    TYPE btcxtime,
             periodic   TYPE btcpflag,
             status     TYPE btcstatus,
             authckman  TYPE btcauthman,
             eventid    TYPE btceventid,
             jobclass   TYPE btcjobclas,
             reaxserver TYPE btcsrvname,

           END OF data_type,

           result_type TYPE yddg_sm37_rb,

           BEGIN OF metrics_type,
             status TYPE c LENGTH 1,
             metric TYPE REF TO yddg__metric,
           END OF metrics_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metrics      TYPE STANDARD TABLE OF metrics_type,
          selector     TYPE string,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_sm37 IMPLEMENTATION.


  METHOD build_tags.

    APPEND row-tag                    TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'       val = row-authckman                                                         ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'USER_ID'      val = row-lastchname                                                        ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'JOB_NAME '    val = row-jobname                                                           ) TO tags.  " sap_job_name
    APPEND yddg__tags=>resolve( handle = 'JOB_CLASS'    val = row-jobclass                                                          ) TO tags.  " sap_job_class
    APPEND yddg__tags=>resolve( handle = 'ASHOST'       val = row-reaxserver                                                        ) TO tags.  " sap_instance
    APPEND yddg__tags=>resolve( handle = 'IS_PERIODIC'  val = SWITCH char05( row-periodic WHEN abap_true THEN 'true' ELSE 'false' ) ) TO tags.  " sap_periodic_job
    APPEND yddg__tags=>resolve( handle = 'EVENT_ID'     val = row-eventid                                                           ) TO tags.  " sap_event_id

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    DATA: datetime           TYPE yddg_datetime,
          metric             TYPE REF TO  yddg__metric,
          where_clause_parts TYPE STANDARD TABLE OF string.

    IF status_running = abap_true.
      APPEND VALUE #( status = 'R'  metric = get_metric( handle = 'SM37_R'  tags = tags  trace_active = trace_active ) ) TO metrics.
      APPEND |STATUS = 'R'| TO where_clause_parts.
    ENDIF.
    IF status_finished = abap_true.
      metric = get_metric( handle = 'SM37_F'  tags = tags  trace_active = trace_active ).
      APPEND VALUE #( status = 'F'  metric = metric ) TO metrics.
      datetime = yddg=>system( metric->last_run ).
      APPEND |( STATUS = 'F' AND ( ( ENDDATE = '{ datetime-date }' AND ENDTIME > '{ datetime-time }' ) OR ENDDATE > '{ datetime-date }' ) )| TO where_clause_parts.
    ENDIF.
    IF status_aborted  = abap_true.
      metric = get_metric( handle = 'SM37_A'  tags = tags  trace_active = trace_active ).
      APPEND VALUE #( status = 'A'  metric = metric ) TO metrics.
      datetime = yddg=>system( metric->last_run ).
      APPEND |( STATUS = 'A' AND ( ( ENDDATE = '{ datetime-date }' AND ENDTIME > '{ datetime-time }' ) OR ENDDATE > '{ datetime-date }' ) )| TO where_clause_parts.
    ENDIF.

    selector = concat_lines_of( table = where_clause_parts  sep = | OR | ).

    CLEAR: where_clause_parts.
    APPEND |( { selector } )| TO where_clause_parts.
    IF job_name_range IS NOT INITIAL.
      APPEND |jobname in JOB_NAME_RANGE| TO where_clause_parts.
    ENDIF.
    IF user_range IS NOT INITIAL.
      APPEND |lastchname in USER_RANGE| TO where_clause_parts.
    ENDIF.

    selector = concat_lines_of( table = where_clause_parts  sep = | AND | ).

    IF trace_active = abap_true.
      WRITE: selector.
    ENDIF.

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

  ENDMETHOD.


  METHOD extract.

    IF selector IS NOT INITIAL.

      SELECT jobname,
             lastchname,
             strtdate,
             strttime,
             enddate,
             endtime,
             periodic,
             status,
             authckman,
             eventid,
             jobclass,
             reaxserver
        FROM tbtco
        WHERE (selector)
        INTO CORRESPONDING FIELDS OF TABLE @data_table.

    ENDIF.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: duration   TYPE i,
          result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         other_tag   = other_tag ).



    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      IF <d>-status = 'R'.
        duration = timestamp - yddg=>timestamp( datetime_system = VALUE yddg_datetime( date = <d>-strtdate
                                                                                       time = <d>-strttime ) ).
      ELSE.
        duration = yddg=>timestamp( datetime_system = VALUE yddg_datetime( date = <d>-enddate
                                                                           time = <d>-endtime ) )
                 - yddg=>timestamp( datetime_system = VALUE yddg_datetime( date = <d>-strtdate
                                                                           time = <d>-strttime ) ).
      ENDIF.
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-tag = brackets->secs( duration ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          tag        = <r>-tag
          jobname    = <r>-jobname
          lastchname = <r>-lastchname
          periodic   = <r>-periodic
          status     = <r>-status
          authckman  = <r>-authckman
          eventid    = <r>-eventid
          jobclass   = <r>-jobclass
          reaxserver = <r>-reaxserver
          value   = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag        = row->tag
          jobname    = row->jobname
          lastchname = row->lastchname
          periodic   = row->periodic
          status     = row->status
          authckman  = row->authckman
          eventid    = row->eventid
          jobclass   = row->jobclass
          reaxserver = row->reaxserver
          value      = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT tag,
           jobname,
           lastchname,
           periodic,
           status,
           authckman,
           eventid,
           jobclass,
           reaxserver
      FROM yddg_sm37_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag        = <r>-tag
          jobname    = <r>-jobname
          lastchname = <r>-lastchname
          periodic   = <r>-periodic
          status     = <r>-status
          authckman  = <r>-authckman
          eventid    = <r>-eventid
          jobclass   = <r>-jobclass
          reaxserver = <r>-reaxserver ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: issue_flag TYPE abap_bool.

    MODIFY yddg_sm37_rb FROM TABLE result_table.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      LOOP AT result_table ASSIGNING <r> WHERE status = <m>-status.

        <m>-metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).

      ENDLOOP.

      TRY.
        <m>-metric->send( ).
        CATCH yddg_x_metric_not_submitted INTO DATA(cx).
          MESSAGE cx TYPE yddg=>submit_error_severity.
          issue_flag = abap_true.
      ENDTRY.


    ENDLOOP.

    IF issue_flag = abap_false.
      DELETE FROM yddg_sm37_rb WHERE value = 0.
    ENDIF.

  ENDMETHOD.


  METHOD finalize.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      <m>-metric->close( timestamp ).

    ENDLOOP.

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
