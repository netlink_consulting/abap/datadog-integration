CLASS yddg__p_sm13 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC.

  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt,
           user_range_type   TYPE RANGE OF xubname.

    CLASS-METHODS:

      class_constructor,

      resolve_rc  IMPORTING !vbrc      TYPE vbrc
                            !vbcliinfo TYPE thraw1
                  RETURNING VALUE(val) TYPE yddg_sm13_rb-status.

    METHODS: constructor IMPORTING !client_range TYPE client_range_type OPTIONAL
                                   !user_range   TYPE user_range_type   OPTIONAL
                                   !bracket_set  TYPE yddg_bracket      DEFAULT 'AGE'
                                   !other_tag    TYPE yddg_tag          DEFAULT 'sap_age:other'
                                   !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active TYPE abap_bool         OPTIONAL.



  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             vbmandt   TYPE vbmandt,
             vbusr     TYPE vbbname,
             vbreport  TYPE vbreport,
             vbtcode   TYPE vbtcode,
             vbrc      TYPE vbrc,
             vbcliinfo TYPE thraw1,
             vbdate    TYPE vbdate,
             vbzonlo   TYPE vbzonlo,
           END OF data_type,

           result_type TYPE yddg_sm13_rb,

           BEGIN OF rc_lookup_type,
             k TYPE vbrc,
             v TYPE yddg_sm13_rb-status,
           END OF rc_lookup_type,

           BEGIN OF selector_type,
             client_range TYPE client_range_type,
             user_range   TYPE user_range_type,
           END OF selector_type.

    CLASS-DATA: tag_lookup TYPE SORTED TABLE OF yddg_tags WITH UNIQUE KEY handle.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    CLASS-DATA: rc_lookup  TYPE SORTED TABLE OF rc_lookup_type WITH UNIQUE KEY k.

    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric       TYPE REF TO yddg__metric,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

private section.
ENDCLASS.



CLASS YDDG__P_SM13 IMPLEMENTATION.


  METHOD build_tags.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'         val = row-vbmandt                                      ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'USER_ID'        val = row-vbusr                                        ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'REPORT'         val = row-vbreport                                     ) TO tags.  " sap_report
    APPEND yddg__tags=>resolve( handle = 'UPDATER_STATUS' val = tag_lookup[ handle = |SM13_{ row-status }| ]-tag ) TO tags.  " sap_updater_status
    APPEND yddg__tags=>resolve( handle = 'TCODE'          val = row-vbtcode                                      ) TO tags.  " sap_tcode

    IF row-status >= 40.
      APPEND |{ tag_lookup[ handle = 'STATUS_ERROR' ]-tag }| TO tags.  " status:error
    ELSEIF row-status >= 20.
      APPEND |{ tag_lookup[ handle = 'STATUS_WARN' ]-tag }|  TO tags.  " status:warn
    ELSE.
      APPEND |{ tag_lookup[ handle = 'STATUS_INFO' ]-tag }|  TO tags.  " status:info
    ENDIF.

  ENDMETHOD.


  METHOD class_constructor.

    rc_lookup = VALUE #( ( k =   0   v =  0 )    " ok                             0  = processed
                         ( k =   1   v =  1 )    " v1 ok                          1  = v1 processed
                         ( k =  12   v = 20 )    " run stopped                    2  = started
                         ( k =  19   v = 40 )    " external abort                 3  = prepared
                         ( k =  21   v = 41 )    " enqueues deleted               4  = v2 processed
                         ( k = 200   v = 21 )    " v2 err                         5  = auto (dia)
                         ( k = 201   v = 22 )    " col err                        6  = auto (sys)
                         ( k = 242   v =  2 )    " run col                        7  = initial
                         ( k = 244   v =  3 )    " external prepared             20  = stopped (no retry)                    W
                         ( k = 245   v =  4 )    " v2 processed                  21  = error in v2 part                      W
                         ( k = 246   v =  2 )    " run v1                        22  = error in collection run               W
                         ( k = 247   v =  4 )    " executed                      23  = to delete                             W
                         ( k = 248   v = 23 )    " autodelete                    40  = canceled                              E
                         ( k = 249   v =  2 )    " restart v2                    41  = enqueues deleted                      E
                         ( k = 250   v =  2 )    " restart v1                    42  = error                                 E
                         ( k = 251   v = 23 )    " delete                        43  = error (external Data/Communication)   E
                         ( k = 252   v =  2 )    " run v2                        44  = error (no retry)                      E
                         ( k = 253   v =  5 )    " autodiaexec
                         ( k = 254   v =  6 )    " autosysecex
                         ( k = 255   v =  7 ) ). " notexecuted

    SELECT handle,
           tag
      FROM yddg_tags
      WHERE handle LIKE 'STATUS_%'
        OR  handle LIKE 'SM13_%'
      INTO TABLE @tag_lookup.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

    metric = get_metric( handle = 'SM13'  tags  = tags  trace_active = trace_active ).

    selector-client_range = client_range.
    selector-user_range   = user_range.

  ENDMETHOD.


  METHOD extract.

    SELECT vbmandt,
           vbusr,
           vbreport,
           vbtcode,
           vbrc,
           vbcliinfo,
           vbdate,
           vbzonlo
      FROM vbhdr
      WHERE vbmandt IN @selector-client_range
        AND vbusr   IN @selector-user_range
      INTO CORRESPONDING FIELDS OF TABLE @data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT tag,
           vbmandt,
           vbusr,
           vbreport,
           vbtcode,
           status
      FROM yddg_sm13_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag      = <r>-tag
          vbmandt  = <r>-vbmandt
          vbusr    = <r>-vbusr
          vbreport = <r>-vbreport
          vbtcode  = <r>-vbtcode
          status   = <r>-status  ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_sm13_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_sm13_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD resolve_rc.

    CONSTANTS: vb_dont_process_again TYPE x VALUE 2,
               vb_external_data      TYPE x VALUE 32.


    TRY.
        val = rc_lookup[ k = vbrc ]-v.
      CATCH cx_sy_itab_line_not_found.
        IF vbcliinfo O vb_external_data.
          val = 43.
        ELSEIF vbcliinfo O vb_dont_process_again.
          val = 44.
        ELSE.
          val = 42.
        ENDIF.
    ENDTRY.

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).



    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      DATA(datetime) = yddg=>datetime( CONV #( <d>-vbdate ) ).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_timezone = VALUE #( date     = datetime-date
                                                                                      time     = datetime-time
                                                                                      timezone = <d>-vbzonlo ) ) ).
      result_row-status = resolve_rc( vbrc = <d>-vbrc
                                      vbcliinfo = <d>-vbcliinfo ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          tag      = <r>-tag
          vbmandt  = <r>-vbmandt
          vbusr    = <r>-vbusr
          vbreport = <r>-vbreport
          vbtcode  = <r>-vbtcode
          status   = <r>-status
          value   = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag      = row->tag
          vbmandt  = row->vbmandt
          vbusr    = row->vbusr
          vbreport = row->vbreport
          vbtcode  = row->vbtcode
          status   = row->status
          value    = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.
ENDCLASS.
