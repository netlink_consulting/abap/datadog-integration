*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LYDDG_T_MAINTENANTOP.              " Global Declarations
  INCLUDE LYDDG_T_MAINTENANUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LYDDG_T_MAINTENANF...              " Subroutines
* INCLUDE LYDDG_T_MAINTENANO...              " PBO-Modules
* INCLUDE LYDDG_T_MAINTENANI...              " PAI-Modules
* INCLUDE LYDDG_T_MAINTENANE...              " Events
* INCLUDE LYDDG_T_MAINTENANP...              " Local class implement.
* INCLUDE LYDDG_T_MAINTENANT99.              " ABAP Unit tests
  INCLUDE LYDDG_T_MAINTENANF00                    . " subprograms
  INCLUDE LYDDG_T_MAINTENANI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
