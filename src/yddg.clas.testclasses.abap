CLASS test_yddg DEFINITION FOR TESTING
DURATION SHORT RISK LEVEL HARMLESS.

  PRIVATE SECTION.

    CONSTANTS: artemis_liftoff_ts   TYPE yddg_ts VALUE 1668581264,
               artemis_liftoff_abap TYPE timestamp VALUE '20221116064744',
               BEGIN OF  artemis_liftoff_utc,
                 date TYPE d VALUE '20221116',
                 time TYPE t VALUE '064744',
               END OF artemis_liftoff_utc,
               BEGIN OF  artemis_liftoff_est,
                 date     TYPE d VALUE '20221116',
                 time     TYPE t VALUE '014744',
                 timezone TYPE tznzone VALUE 'EST',
               END OF artemis_liftoff_est.


    CLASS-DATA: artemis_liftoff_user   TYPE yddg_datetime_user,
                artemis_liftoff_system TYPE yddg_datetime.

    CLASS-METHODS: class_setup.

    METHODS:
      test_abap_timestamp   FOR TESTING,
      test_datetime         FOR TESTING,

      test_utc_now          FOR TESTING,
      test_utc_timestamp    FOR TESTING,
      test_utc_system       FOR TESTING,
      test_utc_user         FOR TESTING,
      test_utc_zone         FOR TESTING,

      test_timestamp_now    FOR TESTING,
      test_timestamp_utc    FOR TESTING,
      test_timestamp_system FOR TESTING,
      test_timestamp_user   FOR TESTING,
      test_timestamp_zone   FOR TESTING,

      test_system_now       FOR TESTING,
      test_system_timestamp FOR TESTING,
      test_system_utc       FOR TESTING,
      test_system_user      FOR TESTING,
      test_system_zone      FOR TESTING,

      test_user_now         FOR TESTING,
      test_user_timestamp   FOR TESTING,
      test_user_utc         FOR TESTING,
      test_user_system      FOR TESTING,
      test_user_zone        FOR TESTING,

      test_zone_now         FOR TESTING,
      test_zone_timestamp   FOR TESTING,
      test_zone_utc         FOR TESTING,
      test_zone_system      FOR TESTING,
      test_zone_user        FOR TESTING.

ENDCLASS.



CLASS test_yddg IMPLEMENTATION.

  METHOD class_setup.

    CONVERT TIME STAMP artemis_liftoff_abap
                 TIME ZONE sy-zonlo
            INTO DATE artemis_liftoff_user-date
                 TIME artemis_liftoff_user-time.
    artemis_liftoff_user-user_id = sy-uname.
    SELECT SINGLE tzonesys
      FROM ttzcu
      INTO @DATA(system_timezone).
    CONVERT TIME STAMP artemis_liftoff_abap
                 TIME ZONE system_timezone
            INTO DATE artemis_liftoff_system-date
                 TIME artemis_liftoff_system-time.

  ENDMETHOD.


  METHOD test_abap_timestamp.
    DATA(abap_ts) = yddg=>abap_timestamp( artemis_liftoff_utc ).
    cl_abap_unit_assert=>assert_equals( act = abap_ts  exp = artemis_liftoff_abap ).
  ENDMETHOD.

  METHOD test_datetime.
    DATA(dt) = yddg=>datetime( artemis_liftoff_abap ).
    cl_abap_unit_assert=>assert_equals( act = dt  exp = artemis_liftoff_utc ).
  ENDMETHOD.

  METHOD test_utc_now.
    DATA: datetime_exp TYPE yddg_datetime.
    GET TIME STAMP FIELD DATA(ts).
    CONVERT TIME STAMP ts TIME ZONE 'UTC' INTO DATE datetime_exp-date TIME datetime_exp-time.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( )  exp = datetime_exp ).
  ENDMETHOD.

  METHOD test_utc_timestamp.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( artemis_liftoff_ts )  exp = artemis_liftoff_utc ).
  ENDMETHOD.

  METHOD test_utc_system.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( datetime_system = artemis_liftoff_system )  exp = artemis_liftoff_utc ).
  ENDMETHOD.

  METHOD test_utc_user.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( datetime_user = artemis_liftoff_user )  exp = artemis_liftoff_utc ).
  ENDMETHOD.

  METHOD test_utc_zone.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( datetime_timezone = artemis_liftoff_est )  exp = artemis_liftoff_utc ).
  ENDMETHOD.

  METHOD test_timestamp_now.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( yddg=>timestamp( ) )  exp = yddg=>utc( ) ).
  ENDMETHOD.

  METHOD test_timestamp_utc.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timestamp( artemis_liftoff_utc )  exp = artemis_liftoff_ts ).
  ENDMETHOD.

  METHOD test_timestamp_system.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timestamp( datetime_system = artemis_liftoff_system )  exp = artemis_liftoff_ts ).
  ENDMETHOD.

  METHOD test_timestamp_user.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timestamp( datetime_user = artemis_liftoff_user )  exp = artemis_liftoff_ts ).
  ENDMETHOD.

  METHOD test_timestamp_zone.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timestamp( datetime_timezone = artemis_liftoff_est )  exp = artemis_liftoff_ts ).
  ENDMETHOD.

  METHOD test_system_now.
    cl_abap_unit_assert=>assert_equals( act = yddg=>utc( datetime_system = yddg=>system( ) )  exp = yddg=>utc( ) ).
  ENDMETHOD.

  METHOD test_system_timestamp.
    cl_abap_unit_assert=>assert_equals( act = yddg=>system( artemis_liftoff_ts )  exp = artemis_liftoff_system ).
  ENDMETHOD.

  METHOD test_system_utc.
    cl_abap_unit_assert=>assert_equals( act = yddg=>system( utc = artemis_liftoff_utc )  exp = artemis_liftoff_system ).
  ENDMETHOD.

  METHOD test_system_user.
    cl_abap_unit_assert=>assert_equals( act = yddg=>system( datetime_user = artemis_liftoff_user )  exp = artemis_liftoff_system ).
  ENDMETHOD.

  METHOD test_system_zone.
    cl_abap_unit_assert=>assert_equals( act = yddg=>system( datetime_timezone = artemis_liftoff_est )  exp = artemis_liftoff_system ).
  ENDMETHOD.

  METHOD test_user_now.
    DATA(now) = yddg=>user( ).
    cl_abap_unit_assert=>assert_equals( act = now-date     exp = sy-datum ).
    cl_abap_unit_assert=>assert_equals( act = now-time     exp = sy-uzeit ).
    cl_abap_unit_assert=>assert_equals( act = now-user_id  exp = sy-uname ).
  ENDMETHOD.

  METHOD test_user_timestamp.
    cl_abap_unit_assert=>assert_equals( act = yddg=>user( artemis_liftoff_ts )  exp = artemis_liftoff_user ).
  ENDMETHOD.

  METHOD test_user_utc.
    cl_abap_unit_assert=>assert_equals( act = yddg=>user( utc = artemis_liftoff_utc )  exp = artemis_liftoff_user ).
  ENDMETHOD.

  METHOD test_user_system.
    cl_abap_unit_assert=>assert_equals( act = yddg=>user( datetime_system = artemis_liftoff_system )  exp = artemis_liftoff_user ).
  ENDMETHOD.

  METHOD test_user_zone.
    cl_abap_unit_assert=>assert_equals( act = yddg=>user( datetime_timezone = artemis_liftoff_est )  exp = artemis_liftoff_user ).
  ENDMETHOD.

  METHOD test_zone_now.
    DATA(now) = yddg=>timezone( timezone = sy-zonlo ).
    cl_abap_unit_assert=>assert_equals( act = now-date      exp = sy-datum ).
    cl_abap_unit_assert=>assert_equals( act = now-time      exp = sy-uzeit ).
    cl_abap_unit_assert=>assert_equals( act = now-timezone  exp = sy-zonlo ).
  ENDMETHOD.

  METHOD test_zone_timestamp.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timezone( timestamp = artemis_liftoff_ts  timezone = 'EST' )  exp = artemis_liftoff_est ).
  ENDMETHOD.

  METHOD test_zone_utc.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timezone( utc = artemis_liftoff_utc  timezone = 'EST' )  exp = artemis_liftoff_est ).
  ENDMETHOD.

  METHOD test_zone_system.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timezone( datetime_system = artemis_liftoff_system  timezone = 'EST' )  exp = artemis_liftoff_est ).
  ENDMETHOD.

  METHOD test_zone_user.
    cl_abap_unit_assert=>assert_equals( act = yddg=>timezone( datetime_user = artemis_liftoff_user  timezone = 'EST' )  exp = artemis_liftoff_est ).
  ENDMETHOD.

ENDCLASS.
