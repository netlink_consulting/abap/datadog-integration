CLASS yddg__p_st22 DEFINITION
  PUBLIC
  INHERITING FROM yddg__p
  ABSTRACT
  CREATE PUBLIC .


  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt,
           user_range_type   TYPE RANGE OF xubname,
           ashost_range_type TYPE RANGE OF snap_syinst.

    METHODS:

      constructor IMPORTING !client_range TYPE client_range_type  OPTIONAL
                            !user_range   TYPE user_range_type    OPTIONAL
                            !ashost_range TYPE ashost_range_type  OPTIONAL
                            !tag_range    TYPE yddg_tag_range_tt  OPTIONAL
                            !trace_active TYPE abap_bool .

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             datum TYPE sydatum,
             uzeit TYPE syuzeit,
             ahost TYPE snap_syinst,
             uname TYPE syuname,
             mandt TYPE symandt,
             modno TYPE sywpid,
             ft    TYPE rsdump_ft_it,
           END OF data_type,

           BEGIN OF result_type,
             timestamp      TYPE yddg_ts,
             ahost          TYPE snap_syinst,
             uname          TYPE syuname,
             mandt          TYPE symandt,
             modno          TYPE sywpid,
             runtime_error  TYPE rsdump_ft-value,
             program        TYPE rsdump_ft-value,
             main_program   TYPE rsdump_ft-value,
             batch_jobname  TYPE rsdump_ft-value,
             batch_user     TYPE rsdump_ft-value,
             tcode          TYPE rsdump_ft-value,
             rfc_error      TYPE rsdump_ft-value,
             rfc_connection TYPE rsdump_ft-value,
           END OF result_type,

           BEGIN OF selector_type,
             client_range TYPE client_range_type,
             user_range   TYPE user_range_type,
             ashost_range TYPE ashost_range_type,
           END OF selector_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: event        TYPE REF TO yddg__event,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.


    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_st22 IMPLEMENTATION.


  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'ASHOST'        val = row-ahost          ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'CLIENT'        val = row-mandt          ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'USER_ID'       val = row-uname          ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TCODE'         val = row-tcode          ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'REPORT'        val = row-program        ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'MAIN_PROGRAM'  val = row-main_program   ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'JOB_NAME'      val = row-batch_jobname  ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'JOB_USER'      val = row-batch_user     ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'RFC_ERROR'     val = row-rfc_error      ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'DESTINATION'   val = row-rfc_connection ) TO tags.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    selector-client_range     = client_range.
    selector-user_range       = user_range.
    selector-ashost_range     = ashost_range.

    event = get_event( handle = 'ST22'  tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD extract.

    DATA(start_datetime) = yddg=>system( event->last_run + 1 ).

    SELECT datum, uzeit, ahost, mandt, uname, modno
        FROM snap
        WHERE ( ( datum = @start_datetime-date AND uzeit > @start_datetime-time ) OR datum > @start_datetime-date )
          AND mandt IN @selector-client_range
          AND uname IN @selector-user_range
          AND ahost IN @selector-ashost_range
          AND seqno = '000'
          INTO CORRESPONDING FIELDS OF TABLE @data_table.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      CALL FUNCTION 'RS_ST22_GET_FT'
        EXPORTING
          datum = <d>-datum
          uzeit = <d>-uzeit
          uname = <d>-uname
          ahost = <d>-ahost
          modno = <d>-modno
          mandt = <d>-mandt
        IMPORTING
          ft    = <d>-ft.
    ENDLOOP.

  ENDMETHOD.


  METHOD finalize.

    event->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.


  METHOD load.

    DATA: text TYPE TABLE OF string.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>).

      text = VALUE #( ( |%%% { CL_ABAP_CHAR_UTILITIES=>NEWLINE }| ) ).
      APPEND |# Shortdump { <r>-runtime_error }| TO text.
      APPEND space TO text.
      APPEND |## { yddg=>iso( <r>-timestamp ) }| TO text.
      APPEND space TO text.
      APPEND '|   |   |' TO text.
      APPEND '|---|---|' TO text.
      add_line  <r>-ahost          'Application Server'.
      add_line  <r>-mandt          'Client'.
      add_line  <r>-uname          'User'.
      add_line  <r>-modno          'Workprocess'.
      add_line  <r>-tcode          'Transaction'.
      add_line  <r>-program        'Program'.
      add_line  <r>-main_program   'Main Program'.
      add_line  <r>-batch_jobname  'Background Job'.
      add_line  <r>-batch_user     'Background User'.
      add_line  <r>-rfc_error      'RFC Error'.
      add_line  <r>-rfc_connection 'RFC Destination'.
      APPEND |{ CL_ABAP_CHAR_UTILITIES=>NEWLINE } %%%| TO text.
      event->append( alert_type = 'error'
                     timestamp  = <r>-timestamp
                     priority   = 'normal'
                     text       = concat_lines_of( table = text  sep = CL_ABAP_CHAR_UTILITIES=>NEWLINE )
                     title      = |Shortdump { <r>-runtime_error } in { <r>-program }|
                     tags       = build_tags( <r> ) ).
    ENDLOOP.

    TRY.
        event->send( ).
      CATCH yddg_x_event_not_submitted INTO DATA(cx).
        timestamp = 0.
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-timestamp = yddg=>timestamp( datetime_system = VALUE #( date = <d>-datum
                                                                         time = <d>-uzeit ) ).
      LOOP AT <d>-ft ASSIGNING FIELD-SYMBOL(<ft>).
        CASE <ft>-id.
          WHEN 'FC'.  result_row-runtime_error = <ft>-value.
          WHEN 'AP'.  result_row-program       = <ft>-value.
          WHEN 'BA'.  result_row-batch_jobname = <ft>-value.
          WHEN 'BI'.  result_row-batch_user    = <ft>-value.
          WHEN 'AM'.  result_row-main_program  = <ft>-value.
          WHEN 'TC'.  result_row-tcode         = <ft>-value.
          WHEN 'RE'. IF <ft>-value <> '-'. result_row-rfc_error      = <ft>-value. ENDIF.
          WHEN 'RC'. IF <ft>-value <> '-'. result_row-rfc_connection = <ft>-value. ENDIF.
        ENDCASE.
      ENDLOOP.
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.
ENDCLASS.
