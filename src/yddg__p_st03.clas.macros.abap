DEFINE pivot_memory.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_MEMORY_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE pivot_usertcode.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE pivot_rfc_client.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_RFC_CLIENT_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE pivot_rfc_server.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_RFC_SERVER_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE pivot_web_client.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_WEB_CLIENT_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE pivot_web_server.

  IF &1 IS NOT INITIAL.
    row-pk-metric = |ST03_WEB_SERVER_{ &2 }|.
    row-value = &1.
    APPEND row TO result.
  ENDIF.

END-OF-DEFINITION.


DEFINE init_row.

  CLEAR: row.
  MOVE-CORRESPONDING <d> TO row-pk.
  row-timestamp = timestamp.
  row-pk-instance = instance.

  IF <d>-entry_id+72(1) = 'T'.
    row-pk-tcode = <d>-entry_id(20).
  ELSE.
    row-pk-report = <d>-entry_id(40).
    row-pk-jobname = <d>-entry_id+40(32).
  ENDIF.

END-OF-DEFINITION.
