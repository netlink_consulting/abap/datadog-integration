REPORT yddg_sm66 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'SM66'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

  AUTHORITY-CHECK OBJECT 'S_ADMI_FCD'
                  ID 'S_ADMI_FCD' FIELD 'PADM'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_sm66( tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
