CLASS yddg_x_apikey_not_found DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS: constructor IMPORTING !previous LIKE previous OPTIONAL.

ENDCLASS.



CLASS yddg_x_apikey_not_found IMPLEMENTATION.

  METHOD constructor ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '902' ).

  ENDMETHOD.

ENDCLASS.
