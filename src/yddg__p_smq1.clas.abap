CLASS yddg__p_smq1 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt,
           queue_range_type  TYPE RANGE OF trfcqnam,
           dest_range_type   TYPE RANGE OF rfcdest.

    METHODS: constructor IMPORTING !client_range TYPE client_range_type OPTIONAL
                                   !queue_range  TYPE queue_range_type OPTIONAL
                                   !dest_range   TYPE dest_range_type OPTIONAL
                                   !bracket_set  TYPE yddg_bracket DEFAULT 'AGE'
                                   !other_tag    TYPE yddg_tag DEFAULT 'sap_age:other'
                                   !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             mandt TYPE symandt,
             qname TYPE trfcqnam,
             dest  TYPE rfcdest,
             qdeep TYPE i,
             fdate TYPE d,
             ftime TYPE t,
           END OF data_type,

           result_type TYPE yddg_smq1_rb,

           BEGIN OF selector_type,
             mandt TYPE mandt,
             qname TYPE trfcqnam,
             dest  TYPE rfcdest,
           END OF selector_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.


    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric       TYPE REF TO yddg__metric,
          selector     TYPE TABLE OF selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_smq1 IMPLEMENTATION.

  METHOD build_tags.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'      val = row-mandt ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'DESTINATION' val = row-dest  ) TO tags.  " sap_destination
    APPEND yddg__tags=>resolve( handle = 'QUEUE'       val = row-qname ) TO tags.  " sap_queue

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

    metric = get_metric( handle = 'SMQ1'  tags  = tags  trace_active = trace_active ).

    IF lines( client_range ) > 0 OR
       lines( queue_range )  > 0 OR
       lines( dest_range )   > 0.
      SELECT DISTINCT mandt,
                      qname,
                      dest
        FROM trfcqout CLIENT SPECIFIED
        WHERE mandt IN @client_range
          AND qname IN @queue_range
          AND dest  IN @dest_range
        INTO CORRESPONDING FIELDS OF TABLE @selector.
    ELSE.
      APPEND VALUE #( mandt = '*'
                      qname = '*'
                      dest  = '*' ) TO selector.
    ENDIF.

  ENDMETHOD.


  METHOD extract.

    DATA: qview TYPE TABLE OF trfcqview.

    LOOP AT selector ASSIGNING FIELD-SYMBOL(<s>).
      CLEAR: qview.
      CALL FUNCTION 'TRFC_QOUT_OVERVIEW'
        EXPORTING
          qname          = <s>-qname
          dest           = <s>-dest
          client         = <s>-mandt
          dist_get_queue = 'A'
        TABLES
          qview          = qview.
      MOVE-CORRESPONDING qview TO data_table KEEPING TARGET LINES.
    ENDLOOP.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).



    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-value = <d>-qdeep.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_system = VALUE #( date    = <d>-fdate
                                                                                    time    = <d>-ftime ) ) ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT tag,
           mandt,
           qname,
           dest
      FROM yddg_smq1_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag   = <r>-tag
          mandt = <r>-mandt
          qname = <r>-qname
          dest  = <r>-dest ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_smq1_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_smq1_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
