CLASS yddg_x DEFINITION PUBLIC INHERITING FROM cx_dynamic_check CREATE PUBLIC.

  PUBLIC SECTION.

    INTERFACES if_t100_message .

    METHODS: constructor IMPORTING !textid   LIKE if_t100_message=>t100key OPTIONAL
                                   !previous LIKE previous                 OPTIONAL
                                   !msgid    TYPE symsgid                  DEFAULT 'YDDG'
                                   !msgno    TYPE symsgno                  DEFAULT '000'
                                   !attr1    TYPE scx_attrname             DEFAULT ''
                                   !attr2    TYPE scx_attrname             DEFAULT ''
                                   !attr3    TYPE scx_attrname             DEFAULT ''
                                   !attr4    TYPE scx_attrname             DEFAULT ''.

ENDCLASS.



CLASS yddg_x IMPLEMENTATION.

  METHOD constructor ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous ).

    CLEAR me->textid.
    IF textid IS INITIAL.
      if_t100_message~t100key = VALUE #( msgid = msgid
                                         msgno = msgno
                                         attr1 = attr1
                                         attr2 = attr2
                                         attr3 = attr3
                                         attr4 = attr4 ).
    ELSE.
      if_t100_message~t100key = textid.
    ENDIF.

  ENDMETHOD.

ENDCLASS.
