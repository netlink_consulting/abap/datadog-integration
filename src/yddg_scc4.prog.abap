REPORT yddg_scc4 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: mandt_dummy TYPE mandt.

SELECT-OPTIONS: s_mandt FOR mandt_dummy.

SELECTION-SCREEN SKIP 1.

SELECTION-SCREEN BEGIN OF BLOCK bl1 WITH FRAME TITLE TEXT-001.
PARAMETERS: p_cs TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_cc TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_cp TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_cr TYPE char01 AS CHECKBOX DEFAULT abap_true,
            p_up TYPE char01 AS CHECKBOX DEFAULT abap_true.
SELECTION-SCREEN END OF BLOCK bl1.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
   ID 'TCD' FIELD 'SCC4'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.


START-OF-SELECTION.

  NEW yddg_p_scc4( client_range = s_mandt[]
                   tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
