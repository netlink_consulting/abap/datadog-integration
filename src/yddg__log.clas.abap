CLASS yddg__log DEFINITION PUBLIC CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: handle   TYPE yddg_metric READ-ONLY,
          last_run TYPE yddg_ts READ-ONLY.

    METHODS:

      constructor IMPORTING !handle       TYPE yddg_metric
                            !service      TYPE string            OPTIONAL
                            !tags         TYPE REF TO yddg__tags OPTIONAL
                            !trace_active TYPE abap_bool
                            !package_size TYPE i                 DEFAULT 50,

      append IMPORTING !message TYPE string
                       !tags    TYPE yddg_tag_tt OPTIONAL,

      close IMPORTING !timestamp TYPE yddg_ts,

      send  IMPORTING !tries TYPE i DEFAULT 3
            RAISING   yddg_x_log_not_submitted.


  PROTECTED SECTION.

    TYPES: BEGIN OF item_type,
             ddsource TYPE string,
             ddtags   TYPE string,
             hostname TYPE string,
             message  TYPE string,
             service  TYPE string,
           END OF item_type.

    DATA: source       TYPE string,
          service      TYPE string,
          tags         TYPE REF TO yddg__tags,
          trace_active TYPE abap_bool,
          package_size TYPE i,
          is_closed    TYPE abap_bool VALUE abap_false,
          metric       TYPE REF TO yddg__metric.

    DATA: items TYPE STANDARD TABLE OF item_type.

    METHODS: send_package IMPORTING !package LIKE items
                                    !tries   TYPE i
                          RAISING   yddg_x_log_not_submitted.

ENDCLASS.



CLASS yddg__log IMPLEMENTATION.

  METHOD constructor.

    me->handle       = handle.
    me->service      = service.
    me->tags         = tags.
    me->trace_active = trace_active.
    me->package_size = package_size.
    me->metric       = NEW #( handle = handle ).
    me->source       = me->metric->name.
    me->last_run     = me->metric->last_run.

  ENDMETHOD.


  METHOD append.

    IF is_closed = abap_true.
      MESSAGE e007 WITH handle.
    ENDIF.

    APPEND VALUE #( ddsource = me->source
                    ddtags   = concat_lines_of( table = me->tags->union( tags ) sep = ',' )
                    hostname = yddg=>hostname
                    message  = message
                    service  = me->service ) TO items.

  ENDMETHOD.


  METHOD close.

    IF is_closed = abap_false.

      IF timestamp IS NOT INITIAL.
        SELECT
          SINGLE *
          FROM   yddg_metrics
          WHERE  handle = @metric->handle
          INTO   @DATA(wa).
        wa-timestamp = timestamp.
        MODIFY yddg_metrics FROM wa.
      ENDIF.

      CALL FUNCTION 'DEQUEUE_EYDDG_METRIC'
        EXPORTING
          handle = metric->handle.

      is_closed = abap_true.

    ENDIF.

  ENDMETHOD.


  METHOD send.

    DATA: package LIKE items.

    TRY.
        metric->send( ).
      CATCH yddg_x_metric_not_submitted. " Metric was not submitted
    ENDTRY.

    WHILE lines( items ) > 0.
      CLEAR package.
      IF lines( items ) > package_size.
        APPEND LINES OF items FROM 1 TO package_size TO package.
        DELETE items FROM 1 TO package_size.
      ELSE.
        package = items.
        CLEAR items.
      ENDIF.
      send_package( package = package
                    tries   = tries ).
    ENDWHILE.

  ENDMETHOD.


  METHOD send_package.
    DATA: json        TYPE string,
          http_status TYPE i,
          response    TYPE string.

    json = /ui2/cl_json=>serialize( data = package
                                    compress = abap_true
                                    pretty_name = /ui2/cl_json=>pretty_mode-low_case ).

    REPLACE ALL OCCURRENCES OF REGEX '[[:cntrl:]]' IN json WITH space.

    DATA(client) = yddg=>log_client( ).
    client->request->set_method( if_http_request=>co_request_method_post ).
    client->request->set_cdata( data = json ).
    client->send( timeout = if_http_client=>co_timeout_default ).
    CALL METHOD client->receive
      EXCEPTIONS
        http_communication_failure = 1                " Communication Error
        http_invalid_state         = 2                " Invalid state
        http_processing_failed     = 3                " Error When Processing Method
        OTHERS                     = 4.
    IF sy-subrc <> 0.
      http_status = 41800 + sy-subrc.
    ELSE.
      client->response->get_status( IMPORTING code = http_status ).
    ENDIF.
    IF http_status <> 202.
      response = client->response->get_cdata( ).
    ENDIF.
    client->close( ).
    DATA(rows) = lines( package ).

    IF http_status = 202.
      IF trace_active = abap_true.
        yddg=>write_json( json ).
      ENDIF.
      MESSAGE s004 WITH rows metric->handle.
    ELSE.
      IF tries > 0.
        send_package( package = package
                      tries   = tries - 1 ).
      ELSE.
        SKIP 2.
        WRITE: / 'Log entries', metric->handle.
        WRITE: / 'HTTP_STATUS:', http_status.
        WRITE: / 'Response:', response.
        SKIP 1.
        yddg=>write_json( json ).
        RAISE EXCEPTION TYPE yddg_x_log_not_submitted
          EXPORTING
            count       = rows
            metric      = metric->handle
            http_status = http_status.
      ENDIF.
    ENDIF.

  ENDMETHOD.

ENDCLASS.
