CLASS yddg__p_st03 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC.

  PUBLIC SECTION.

    TYPES: metrics_range_type TYPE RANGE OF yddg_metric.

    METHODS: constructor IMPORTING !metrics_range TYPE metrics_range_type
                                   !resolution    TYPE swnctimeres DEFAULT 60
                                   !tag_range     TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active  TYPE abap_bool OPTIONAL.

  PROTECTED SECTION.

    TYPES: pk_type_tt          TYPE  TABLE OF yddg_st03_pk WITH EMPTY KEY,
           swncaggmemory_tt    TYPE TABLE OF swncaggmemory WITH EMPTY KEY,
           swncaggusertcode_tt TYPE TABLE OF swncaggusertcode WITH EMPTY KEY,
           swncaggrfcclnt_tt   TYPE TABLE OF swncaggrfcclnt WITH EMPTY KEY,
           swncaggrfcsrvr_tt   TYPE TABLE OF swncaggrfcsrvr WITH EMPTY KEY,
           swncaggwebdest_tt   TYPE TABLE OF swncaggwebdest WITH EMPTY KEY,


           BEGIN OF data_type,
             timestamp TYPE yddg_ts,
             instance  TYPE msname2,
             memory    TYPE swncaggmemory_tt,
             usertcode TYPE swncaggusertcode_tt,
             rfcclnt   TYPE swncaggrfcclnt_tt,
             rfcsrvr   TYPE swncaggrfcsrvr_tt,
             websd     TYPE swncaggwebdest_tt,
             webcd     TYPE swncaggwebdest_tt,
           END OF data_type,

           BEGIN OF result_type,
             timestamp TYPE yddg_ts,
             pk        TYPE yddg_st03_pk,
             value     TYPE int8,
           END OF result_type,

           BEGIN OF result_type_flat,
             timestamp TYPE yddg_ts.
             INCLUDE   TYPE yddg_st03_pk.
           TYPES:
             value     TYPE int8,
           END OF result_type_flat,

           result_type_tt TYPE SORTED TABLE OF result_type WITH NON-UNIQUE KEY timestamp,
           result_flat_tt TYPE TABLE OF result_type_flat WITH EMPTY KEY,

           BEGIN OF metrics_type,
             handle TYPE yddg_handle,
             metric TYPE REF TO yddg__metric,
           END OF metrics_type.

    CLASS-DATA: tag_lookup TYPE SORTED TABLE OF yddg_tags WITH UNIQUE KEY handle.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: metrics      TYPE STANDARD TABLE OF metrics_type,
          resolution   TYPE swnctimeres,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE result_type_tt.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

    METHODS:

      transform_memory IMPORTING !timestamp    TYPE yddg_ts
                                 !instance     TYPE msname2
                                 !memory       TYPE swncaggmemory_tt
                       RETURNING VALUE(result) TYPE result_type_tt,

      transform_usertcode IMPORTING !timestamp    TYPE yddg_ts
                                    !instance     TYPE msname2
                                    !usertcode    TYPE swncaggusertcode_tt
                          RETURNING VALUE(result) TYPE result_type_tt,

      transform_rfcclnt IMPORTING !timestamp    TYPE yddg_ts
                                  !instance     TYPE msname2
                                  !rfcclnt      TYPE swncaggrfcclnt_tt
                        RETURNING VALUE(result) TYPE result_type_tt,

      transform_rfcsrvr IMPORTING !timestamp    TYPE yddg_ts
                                  !instance     TYPE msname2
                                  !rfcsrvr      TYPE swncaggrfcsrvr_tt
                        RETURNING VALUE(result) TYPE result_type_tt,

      transform_websd IMPORTING !timestamp    TYPE yddg_ts
                                !instance     TYPE msname2
                                !websd        TYPE swncaggwebdest_tt
                      RETURNING VALUE(result) TYPE result_type_tt,

      transform_webcd IMPORTING !timestamp    TYPE yddg_ts
                                !instance     TYPE msname2
                                !webcd        TYPE swncaggwebdest_tt
                      RETURNING VALUE(result) TYPE result_type_tt,

      load_merge IMPORTING !timestamp    TYPE yddg_ts
                           !records      TYPE result_type_tt
                           !previous     TYPE pk_type_tt
                 RETURNING VALUE(result) TYPE result_type_tt,

      flatten_result IMPORTING !records      TYPE result_type_tt
                     RETURNING VALUE(result) TYPE result_flat_tt.


ENDCLASS.



CLASS yddg__p_st03 IMPLEMENTATION.


  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'ASHOST'              val = row-pk-instance    ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TASK_TYPE'           val = cl_swnc_constants=>translate_tasktype( row-pk-tasktype ) ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'USER_ID'             val = row-pk-account     ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TCODE'               val = row-pk-tcode       ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'REPORT'              val = row-pk-report      ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'JOB_NAME'            val = row-pk-jobname     ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'CLIENT'              val = row-pk-mandt       ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TARGET'              val = row-pk-target      ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'USER_ID_REMOTE'      val = row-pk-userid      ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'DESTINATION_LOCAL'   val = row-pk-local_dest  ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'DESTINATION_REMOTE'  val = row-pk-remot_dest  ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'PROGRAM_NAME'        val = row-pk-prog_name   ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'FUNCTION_NAME'       val = row-pk-func_name   ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'CALLER'              val = row-pk-rfc_caller  ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'HOST'                val = row-pk-host        ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'PORT'                val = row-pk-port        ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'DESTINATION'         val = row-pk-destination ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'PATH'                val = row-pk-path        ) TO tags.
    IF row-pk-protocol IS NOT INITIAL.
      APPEND yddg__tags=>resolve( handle = 'PROTOCOL'            val = cl_swnc_constants=>translate_protocol( row-pk-protocol ) ) TO tags.
    ENDIF.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    LOOP AT metrics_range ASSIGNING FIELD-SYMBOL(<t>) where sign = 'I' and option = 'EQ'.
      APPEND VALUE #( handle = <t>-low  metric = get_metric( handle = <t>-low  tags = tags  trace_active = trace_active ) ) TO metrics.
    ENDLOOP.

    me->resolution = resolution.

  ENDMETHOD.


  METHOD extract.

    DATA: data_row TYPE data_type.

    " get start point
    " minimum last run of all metrics
    DATA(last_run) = REDUCE yddg_ts( INIT ts = timestamp
                                  FOR d IN metrics
                                  NEXT ts = nmin( val1 = ts
                                                  val2 = d-metric->last_run ) ).
    " cannot send more than 1 hour into the past and get the full minute
    DATA(start_ts) = yddg=>minute( nmax( val1 = last_run + 1
                                         val2 = timestamp - ( 59 * 60 ) ) ).

    " get data in resolution size

    DATA(instances) = yddg=>instances( ).

    DO.
      DATA(end_ts) = start_ts + resolution - 1.
      IF end_ts > timestamp.
        EXIT.
      ENDIF.
      DATA(start_utc) = yddg=>utc( start_ts ).
      DATA(end_utc) = yddg=>utc( end_ts ).
      data_row-timestamp = end_ts.
      LOOP AT instances ASSIGNING FIELD-SYMBOL(<i>).
        data_row-instance = <i>.
        CALL FUNCTION 'SWNC_READ_SNAPSHOT'
          DESTINATION <i>
          EXPORTING
            read_start_date = start_utc-date
            read_start_time = start_utc-time
            read_end_date   = end_utc-date
            read_end_time   = end_utc-time
            factor          = 1
            flag_read_astat = abap_true
            time_resolution = resolution
          TABLES
            memory          = data_row-memory
            usertcode       = data_row-usertcode
            rfcclnt         = data_row-rfcclnt
            rfcsrvr         = data_row-rfcsrvr
            websd           = data_row-websd
            webcd           = data_row-webcd
          EXCEPTIONS
            read_error      = 1
            no_data         = 2
            OTHERS          = 3.
        IF sy-subrc = 0.
          APPEND data_row TO data_table.
        ENDIF.
      ENDLOOP.

      start_ts = end_ts + 1.
    ENDDO.

    IF trace_active = abap_true.
      LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
        write_table( name = |data_table-memory ({ yddg=>iso( <d>-timestamp )    } / { <d>-instance })|  tab = <d>-memory ).
        write_table( name = |data_table-usertcode ({ yddg=>iso( <d>-timestamp ) } / { <d>-instance })|  tab = <d>-usertcode ).
        write_table( name = |data_table-rfcclnt ({ yddg=>iso( <d>-timestamp )   } / { <d>-instance })|  tab = <d>-rfcclnt ).
        write_table( name = |data_table-rfcsrvr ({ yddg=>iso( <d>-timestamp )   } / { <d>-instance })|  tab = <d>-rfcsrvr ).
        write_table( name = |data_table-websd ({ yddg=>iso( <d>-timestamp )     } / { <d>-instance })|  tab = <d>-websd ).
        write_table( name = |data_table-webcd ({ yddg=>iso( <d>-timestamp )     } / { <d>-instance })|  tab = <d>-webcd ).
      ENDLOOP.
    ENDIF.

  ENDMETHOD.


  METHOD finalize.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).

      <m>-metric->close( timestamp ).

    ENDLOOP.

    COMMIT WORK.

  ENDMETHOD.


  METHOD load.

    DATA: previous TYPE pk_type_tt,
          current  TYPE pk_type_tt,
          records  TYPE result_type_tt,
          temp     TYPE result_type_tt.

    SELECT *
      FROM yddg_st03_rb
      INTO CORRESPONDING FIELDS OF TABLE @previous.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      CLEAR: records, current.
      LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>) WHERE timestamp = <d>-timestamp.
        APPEND <r> TO records.
      ENDLOOP.
      MOVE-CORRESPONDING records TO current.
      APPEND LINES OF load_merge( timestamp = <d>-timestamp
                                  records   = records
                                  previous  = previous ) TO temp.
      previous = current.
    ENDLOOP.

    DATA: rb_table TYPE TABLE OF yddg_st03_rb,
          rb_row   TYPE yddg_st03_rb.
    LOOP AT current ASSIGNING FIELD-SYMBOL(<c>).
      CLEAR rb_row.
      MOVE-CORRESPONDING <c> TO rb_row.
      rb_row-guid = cl_system_uuid=>create_uuid_c36_static( ).
      APPEND rb_row TO rb_table.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = flatten_result( result_table ) ).

    DATA: issue_flag TYPE abap_bool.

    LOOP AT metrics ASSIGNING FIELD-SYMBOL(<m>).
      LOOP AT result_table ASSIGNING <r> WHERE pk-metric = <m>-handle.
        <m>-metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).
      ENDLOOP.
      TRY.
          <m>-metric->send( ).
        CATCH yddg_x_metric_not_submitted INTO DATA(cx).
          MESSAGE cx TYPE yddg=>submit_error_severity.
          issue_flag = abap_true.
      ENDTRY.
    ENDLOOP.

    IF issue_flag = abap_false.
      DELETE FROM yddg_st03_rb.
    ENDIF.
    INSERT yddg_st03_rb FROM TABLE rb_table.

  ENDMETHOD.


  METHOD load_merge.

    result = records.

    LOOP AT previous ASSIGNING FIELD-SYMBOL(<p>).
      IF NOT line_exists( result[ pk = <p> ] ).
        APPEND VALUE #( timestamp = timestamp
                        pk        = <p> ) TO result.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.


  METHOD transform.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).

      APPEND LINES OF transform_memory( timestamp = <d>-timestamp
                                           instance  = <d>-instance
                                           memory = <d>-memory ) TO result_table.
      APPEND LINES OF transform_usertcode( timestamp = <d>-timestamp
                                           instance  = <d>-instance
                                           usertcode = <d>-usertcode ) TO result_table.
      APPEND LINES OF transform_rfcclnt( timestamp = <d>-timestamp
                                         instance  = <d>-instance
                                         rfcclnt   = <d>-rfcclnt ) TO result_table.
      APPEND LINES OF transform_rfcsrvr( timestamp = <d>-timestamp
                                         instance  = <d>-instance
                                         rfcsrvr   = <d>-rfcsrvr ) TO result_table.
      APPEND LINES OF transform_webcd( timestamp = <d>-timestamp
                                       instance  = <d>-instance
                                       webcd     = <d>-webcd ) TO result_table.
      APPEND LINES OF transform_websd( timestamp = <d>-timestamp
                                       instance  = <d>-instance
                                       websd     = <d>-websd ) TO result_table.


    ENDLOOP.

    write_table( name = 'result_table'  tab = flatten_result( result_table ) ).

  ENDMETHOD.


  METHOD transform_memory.

    DATA: row TYPE result_type.

    LOOP AT memory ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_memory <d>-counter   'STEPS'.
      pivot_memory <d>-memsum    'TOTAL'.
      pivot_memory <d>-privsum   'PRIVATE'.
      pivot_memory <d>-usedbytes 'EXTENDED_TOTAL'.
      pivot_memory <d>-maxbytes  'EXTENDED_MAX'.
      pivot_memory <d>-privcount 'WP_RESERVATIONS'.
      pivot_memory <d>-restcount 'WP_RESTARTS'.

    ENDLOOP.

  ENDMETHOD.


  METHOD transform_usertcode.

    DATA: row TYPE result_type.

    LOOP AT usertcode ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_usertcode <d>-count          'STEPS'.
      pivot_usertcode <d>-respti         'RESPONSE_TIME'.
      pivot_usertcode <d>-procti         'PROCESSING_TIME'.
      pivot_usertcode <d>-cputi          'CPU_TIME'.
      pivot_usertcode <d>-queueti        'WAIT_TIME'.
      pivot_usertcode <d>-rollwaitti     'ROLL_WAIT_TIME'.
      pivot_usertcode <d>-guitime        'GUI_TIME'.
      pivot_usertcode <d>-guicnt         'GUI_ROUNDTRIPS'.
      pivot_usertcode <d>-guinettime     'GUI_NETWORK_TIME'.
      pivot_usertcode <d>-dbp_count      'DB_PROCEDURE_CALLS'.
      pivot_usertcode <d>-dbp_time       'DB_PROCEDURE_TIME'.
      pivot_usertcode <d>-readdircnt     'DB_READS_DIRECT'.
      pivot_usertcode <d>-readdirti      'DB_TIME_DIRECT'.
      pivot_usertcode <d>-readdirbuf     'BUFFER_READS_DIRECT'.
      pivot_usertcode <d>-readdirrec     'DB_RECORDS_DIRECT'.
      pivot_usertcode <d>-readseqcnt     'DB_READS_SEQUENTIAL'.
      pivot_usertcode <d>-readseqti      'DB_TIME_SEQUENTIAL'.
      pivot_usertcode <d>-readseqbuf     'BUFFER_READS_SEEUENTIAL'.
      pivot_usertcode <d>-readseqrec     'DB_RECORDS_SEQUENTIAL'.
      pivot_usertcode <d>-chngcnt        'DB_CHANGES'.
      pivot_usertcode <d>-chngti         'DB_TIME_CHANGES'.
      pivot_usertcode <d>-chngrec        'DB_RECORDS_CHANGED'.
      pivot_usertcode <d>-phyreadcnt     'DB_PHYSICAL_READS'.
      pivot_usertcode <d>-phychngrec     'DB_PHYSICAL_CHANGES'.
      pivot_usertcode <d>-phycalls       'DB_PHYSICAL_CALLS'.
      pivot_usertcode <d>-vmc_call_count 'VMC_CALLS'.
      pivot_usertcode <d>-vmc_cpu_time   'VMC_CPU_TIME'.
      pivot_usertcode <d>-vmc_elap_time  'VMC_TIME'.

      DATA(trans_bytes) = ( <d>-sli_cnt + <d>-quecnt + <d>-dsqlcnt ) / 1024.
      pivot_usertcode trans_bytes    'TRANSFERRED_BYTES'.

    ENDLOOP.

  ENDMETHOD.


  METHOD flatten_result.

    DATA: row TYPE result_type_flat.

    LOOP AT records ASSIGNING FIELD-SYMBOL(<r>).
      MOVE-CORRESPONDING <r> TO row.
      MOVE-CORRESPONDING <r>-pk TO row.
      APPEND row TO result.
    ENDLOOP.

  ENDMETHOD.

  METHOD transform_rfcclnt.

    DATA: row TYPE result_type.

    LOOP AT rfcclnt ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_rfc_client <d>-counter   'STEPS'.
      pivot_rfc_client <d>-receive   'RECEIVED'.
      pivot_rfc_client <d>-send      'SENT'.
      pivot_rfc_client <d>-exe_time  'EXECUTION_TIME'.
      pivot_rfc_client <d>-call_time 'CALL_TIME'.

    ENDLOOP.

  ENDMETHOD.

  METHOD transform_rfcsrvr.

    DATA: row TYPE result_type.

    LOOP AT rfcsrvr ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_rfc_server <d>-counter   'STEPS'.
      pivot_rfc_server <d>-receive   'RECEIVED'.
      pivot_rfc_server <d>-send      'SENT'.
      pivot_rfc_server <d>-exe_time  'EXECUTION_TIME'.
      pivot_rfc_server <d>-call_time 'CALL_TIME'.

    ENDLOOP.

  ENDMETHOD.

  METHOD transform_webcd.

    DATA: row TYPE result_type.

    LOOP AT webcd ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_web_client <d>-counter           'STEPS'.
      pivot_web_client <d>-calltime          'CALL_TIME'.
      pivot_web_client <d>-execution_ti      'EXECUTION_TIME'.
      pivot_web_client <d>-data_receive      'RECEIVED'.
      pivot_web_client <d>-data_send         'SENT'.
      pivot_web_client <d>-data_receive_ti   'RECEIVE_TIME'.
      pivot_web_client <d>-data_send_ti      'SEND_TIME'.
      pivot_web_client <d>-logon_ti          'LOGON_TIME'.
      pivot_web_client <d>-authorization_ti  'AUTHORIZATION_TIME'.
    ENDLOOP.

  ENDMETHOD.

  METHOD transform_websd.

    DATA: row TYPE result_type.

    LOOP AT websd ASSIGNING FIELD-SYMBOL(<d>).
      init_row.

      pivot_web_server <d>-counter           'STEPS'.
      pivot_web_server <d>-calltime          'CALL_TIME'.
      pivot_web_server <d>-execution_ti      'EXECUTION_TIME'.
      pivot_web_server <d>-data_receive      'RECEIVED'.
      pivot_web_server <d>-data_send         'SENT'.
      pivot_web_server <d>-data_receive_ti   'RECEIVE_TIME'.
      pivot_web_server <d>-data_send_ti      'SEND_TIME'.
      pivot_web_server <d>-logon_ti          'LOGON_TIME'.
      pivot_web_server <d>-authorization_ti  'AUTHORIZATION_TIME'.
    ENDLOOP.


  ENDMETHOD.

ENDCLASS.
