CLASS yddg__p_sm12 DEFINITION
  PUBLIC
  INHERITING FROM yddg__p
  ABSTRACT
  CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt,
           user_range_type   TYPE RANGE OF xubname.

    CLASS-METHODS: class_constructor.

    METHODS: constructor:     IMPORTING
                                !client_range TYPE client_range_type OPTIONAL
                                !user_range   TYPE user_range_type OPTIONAL
                                !bracket_set  TYPE yddg_bracket DEFAULT 'AGE'
                                !other_tag    TYPE yddg_tag DEFAULT 'sap_age:other'
                                !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                !trace_active TYPE abap_bool OPTIONAL .
  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             gname   TYPE eqegraname,
             gmode   TYPE eqegramode,
             gobj    TYPE eqeobj,
             gclient TYPE eqeclient,
             guname  TYPE eqeuname,
             gtcode  TYPE eqetcode,
             gtdate  TYPE eqedate,
             gttime  TYPE eqetime,
           END OF data_type,

           result_type TYPE yddg_sm12_rb,

           BEGIN OF selector_type,
             clients  TYPE TABLE OF mandt WITH EMPTY KEY,
             user_ids TYPE TABLE OF xubname WITH EMPTY KEY,
           END OF selector_type.

    CLASS-DATA: tag_lookup TYPE SORTED TABLE OF yddg_tags WITH UNIQUE KEY handle.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric       TYPE REF TO yddg__metric,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_sm12 IMPLEMENTATION.


  METHOD build_tags.

    DATA(mode) = tag_lookup[ handle = |SM12_{ row-gmode }| ]-tag.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'       val = row-gclient                                     ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'USER_ID'      val = row-guname                                      ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'TABNAME'      val = row-gname                                       ) TO tags.  " sap_table
    APPEND yddg__tags=>resolve( handle = 'LOCK_MODE'    val = tag_lookup[ handle = |SM12_{ row-gmode }| ]-tag ) TO tags.  " sap_lock_mode
    APPEND yddg__tags=>resolve( handle = 'LOCK_OBJECT'  val = row-gobj                                        ) TO tags.  " sap_lock_object
    APPEND yddg__tags=>resolve( handle = 'TCODE'        val = row-gtcode                                      ) TO tags.  " sap_tcode

  ENDMETHOD.


  METHOD class_constructor.

    SELECT handle,
          tag
     FROM yddg_tags
     WHERE handle LIKE 'SM12_%'
     INTO TABLE @tag_lookup.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

    metric = get_metric( handle = 'SM12'  tags  = tags  trace_active = trace_active ).

    IF lines( client_range ) > 0.
      SELECT mandt
        FROM t000
        WHERE mandt IN @client_range
        INTO TABLE @selector-clients.
    ELSE.
      APPEND space TO selector-clients.
    ENDIF.
    IF lines( user_range ) > 0.
      SELECT bname
        FROM usr02
        WHERE bname IN @user_range
        INTO TABLE @selector-user_ids.
    ELSE.
      APPEND space TO selector-user_ids.
    ENDIF.

  ENDMETHOD.


  METHOD extract.

    DATA: enq_table TYPE STANDARD TABLE OF seqg3.

    LOOP AT selector-clients ASSIGNING FIELD-SYMBOL(<client>).
      LOOP AT selector-user_ids ASSIGNING FIELD-SYMBOL(<user_id>).
        CLEAR: enq_table.
        CALL FUNCTION 'ENQUEUE_READ'
          EXPORTING
            gclient = <client>
            guname  = <user_id>
          TABLES
            enq     = enq_table.
        MOVE-CORRESPONDING enq_table TO data_table KEEPING TARGET LINES.
      ENDLOOP.
    ENDLOOP.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).



    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_user = VALUE #( date    = <d>-gtdate
                                                                                  time    = <d>-gttime
                                                                                  user_id = <d>-guname ) ) ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          tag     = <r>-tag
          gname   = <r>-gname
          gmode   = <r>-gmode
          gobj    = <r>-gobj
          gclient = <r>-gclient
          guname  = <r>-guname
          gtcode  = <r>-gtcode
          value   = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag     = row->tag
          gname   = row->gname
          gmode   = row->gmode
          gobj    = row->gobj
          gclient = row->gclient
          guname  = row->guname
          gtcode  = row->gtcode
          value   = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT tag,
           gname,
           gmode,
           gobj,
           gclient,
           guname,
           gtcode
      FROM yddg_sm12_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag     = <r>-tag
          gname   = <r>-gname
          gmode   = <r>-gmode
          gobj    = <r>-gobj
          gclient = <r>-gclient
          guname  = <r>-guname
          gtcode  = <r>-gtcode ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_sm12_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_sm12_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
