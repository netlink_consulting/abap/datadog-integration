FUNCTION CONVERSION_EXIT_YDDGL_INPUT.
*"--------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(INPUT)
*"  EXPORTING
*"     VALUE(OUTPUT)
*"--------------------------------------------------------------------

output = to_upper( input ).

ENDFUNCTION.
