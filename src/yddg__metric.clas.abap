CLASS yddg__metric DEFINITION PUBLIC CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: handle   TYPE yddg_metric READ-ONLY,
          name     TYPE yddg_metric_name READ-ONLY,
          last_run TYPE yddg_ts READ-ONLY.

    METHODS:

      "! <p class="shorttext synchronized" lang="en">Get Metric and enqueue (lock)</p>
      "!
      "! @parameter handle | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tags | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter trace_active | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter package_size | <p class="shorttext synchronized" lang="en"></p>
      constructor IMPORTING !handle       TYPE yddg_metric
                            !tags         TYPE REF TO yddg__tags OPTIONAL
                            !trace_active TYPE abap_bool DEFAULT abap_false
                            !package_size TYPE i DEFAULT 100,

      "! <p class="shorttext synchronized" lang="en">Add datapoint to metric</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter value | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tags | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter all_tags | <p class="shorttext synchronized" lang="en"></p>
      append IMPORTING !timestamp      TYPE yddg_ts
                       !value          TYPE int8
                       !tags           TYPE yddg_tag_tt OPTIONAL
             RETURNING VALUE(all_tags) TYPE yddg_tag_tt,

      "! <p class="shorttext synchronized" lang="en">dequeue (unlock)</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en"></p>
      close IMPORTING      !timestamp TYPE yddg_ts,

      "! <p class="shorttext synchronized" lang="en">Send metric to Datadog</p>
      "!
      "! @parameter tries | <p class="shorttext synchronized" lang="en">Number of retries</p>
      send IMPORTING !tries TYPE i DEFAULT 3
           RAISING   yddg_x_metric_not_submitted.

  PROTECTED SECTION.

    TYPES: BEGIN OF resource_type,
             name TYPE string,
             type TYPE string,
           END OF resource_type,

           BEGIN OF point_type,
             timestamp TYPE yddg_ts,
             value     TYPE int8,
           END OF point_type,

           BEGIN OF item_type,
             metric    TYPE string,
             type      TYPE i,
             points    TYPE TABLE OF point_type    WITH NON-UNIQUE DEFAULT KEY,
             resources TYPE TABLE OF resource_type WITH NON-UNIQUE DEFAULT KEY,
             tags      TYPE yddg_tag_tt,
           END OF item_type.

    DATA: items     TYPE STANDARD TABLE OF item_type,
          resources TYPE STANDARD TABLE OF resource_type.

    DATA: metric_type  TYPE int1,
          tags         TYPE REF TO yddg__tags,
          trace_active TYPE abap_bool,
          package_size TYPE i,
          is_closed    TYPE abap_bool VALUE abap_false.

    METHODS:

      "! <p class="shorttext synchronized" lang="en">Send package of datapoints</p>
      "!
      "! @parameter package | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter tries | <p class="shorttext synchronized" lang="en"></p>
      send_package IMPORTING !package LIKE items
                             !tries   TYPE i
                   RAISING   yddg_x_metric_not_submitted.

ENDCLASS.



CLASS yddg__metric IMPLEMENTATION.


  METHOD append.

    IF is_closed = abap_true.
      MESSAGE e008 WITH handle.
    ENDIF.

    all_tags = me->tags->union( tags ).

    APPEND VALUE #( metric    = me->name
                    type      = me->metric_type
                    points    = VALUE #( ( timestamp = timestamp
                                           value     = value ) )
                    resources = resources
                    tags      = all_tags ) TO items.

  ENDMETHOD.


  METHOD close.

    IF is_closed = abap_false.

      IF timestamp IS NOT INITIAL.
        SELECT
          SINGLE *
          FROM   yddg_metrics
          WHERE  handle = @handle
          INTO   @DATA(wa).
        wa-timestamp = timestamp.
        MODIFY yddg_metrics FROM wa.
      ENDIF.

      CALL FUNCTION 'DEQUEUE_EYDDG_METRIC'
        EXPORTING
          handle = handle.

      is_closed = abap_true.

    ENDIF.

  ENDMETHOD.


  METHOD constructor.

    SELECT
      SINGLE name,
             metric_type,
             timestamp
      FROM   yddg_metrics
      WHERE  handle = @handle
      INTO   @DATA(wa).
    IF sy-subrc <> 0.
      MESSAGE e005 WITH handle.
    ENDIF.

    me->handle       = handle.
    me->name         = wa-name.
    IF wa-timestamp IS INITIAL.
      wa-timestamp = yddg=>timestamp( ) - ( 18 * 60 * 60 ).
    ENDIF.
    me->last_run     = wa-timestamp.
    me->metric_type  = wa-metric_type.
    me->tags         = tags.
    me->trace_active = trace_active.
    me->package_size = package_size.

    IF yddg=>hostname IS NOT INITIAL.
      APPEND VALUE #( name = yddg=>hostname
                      type = 'host' ) TO me->resources.
    ENDIF.

    APPEND VALUE #( metric    = 'datadog.marketplace.netlink.sap_abap'
                    type      = me->metric_type
                    points    = VALUE #( ( timestamp = yddg=>timestamp( )
                                           value     = 1 ) )
                    tags      = VALUE #( ( yddg=>unit_tag ) ) ) TO items.

  ENDMETHOD.


  METHOD send.

    DATA: package LIKE items.

    WHILE lines( items ) > 0.
      CLEAR package.
      IF lines( items ) > package_size.
        APPEND LINES OF items FROM 1 TO package_size TO package.
        DELETE items FROM 1 TO package_size.
      ELSE.
        package = items.
        CLEAR items.
      ENDIF.
      send_package( package = package
                    tries   = tries ).
    ENDWHILE.

  ENDMETHOD.


  METHOD send_package.
    DATA: json        TYPE string,
          http_status TYPE i,
          response    TYPE string.

    json = /ui2/cl_json=>serialize( data = package
                                    name = 'series'
                                    compress = abap_true
                                    pretty_name = /ui2/cl_json=>pretty_mode-low_case ).
    REPLACE ALL OCCURRENCES OF REGEX '[[:cntrl:]]' IN json WITH space.
    json = '{' && json && '}'.
    DATA(client) = yddg=>api_client( 'v2/series' ).
    client->request->set_method( if_http_request=>co_request_method_post ).
    client->request->set_cdata( data = json ).
    client->send( timeout = if_http_client=>co_timeout_default ).
    CALL METHOD client->receive
      EXCEPTIONS
        http_communication_failure = 1                " Communication Error
        http_invalid_state         = 2                " Invalid state
        http_processing_failed     = 3                " Error When Processing Method
        OTHERS                     = 4.
    IF sy-subrc <> 0.
      http_status = 41800 + sy-subrc.
    ELSE.
      client->response->get_status( IMPORTING code = http_status ).
    ENDIF.
    IF http_status <> 202.
      response = client->response->get_cdata( ).
    ENDIF.
    client->close( ).
    DATA(rows) = lines( package ).

    IF http_status = 202.
      IF trace_active = abap_true.
        yddg=>write_json( json ).
      ENDIF.
      MESSAGE s006 WITH rows me->handle.
    ELSE.
      IF tries > 0.
        send_package( package = package
                      tries   = tries - 1 ).
      ELSE.
        SKIP 2.
        WRITE: / 'Cannot submit metrics', me->handle.
        WRITE: / 'HTTP_STATUS:', http_status.
        WRITE: / 'Response:', response.
        SKIP 1.
        yddg=>write_json( json ).
        RAISE EXCEPTION TYPE yddg_x_metric_not_submitted
          EXPORTING
            count       = rows
            metric      = me->handle
            http_status = http_status.
      ENDIF.
    ENDIF.

  ENDMETHOD.

ENDCLASS.
