CLASS yddg__p_sm21 DEFINITION
  PUBLIC
  INHERITING FROM yddg__p
  ABSTRACT
  CREATE PUBLIC .


  PUBLIC SECTION.

    CLASS-METHODS:
      class_constructor.

    METHODS:
      constructor IMPORTING !messsageid_range TYPE cl_syslog_filter=>range_of_msgid    OPTIONAL
                            !client_range     TYPE cl_syslog_filter=>range_of_mandt    OPTIONAL
                            !user_range       TYPE cl_syslog_filter=>range_of_user     OPTIONAL
                            !tcode_range      TYPE cl_syslog_filter=>range_of_tcode    OPTIONAL
                            !wptype_range     TYPE cl_syslog_filter=>range_of_types    OPTIONAL
                            !prog_range       TYPE cl_syslog_filter=>range_of_progids  OPTIONAL
                            !pack_range       TYPE cl_syslog_filter=>range_of_devclass OPTIONAL
                            !tag_range        TYPE yddg_tag_range_tt                   OPTIONAL
                            !trace_active     TYPE abap_bool .

  PROTECTED SECTION.

    TYPES: BEGIN OF syslog_category_type,
             rslgnkky TYPE rslgnkkd,
             rslgnkva TYPE rslgnkvd,
           END OF syslog_category_type,

           BEGIN OF syslog_class_type,
             domvalue_l TYPE rslgclasid,
             ddtext     TYPE val_text,
           END OF syslog_class_type,

           BEGIN OF syslog_type_type,
             sltype TYPE rslgtype_d,
             txt    TYPE rslgmsgtyp,
           END OF syslog_type_type,


           BEGIN OF result_type,
             utc             TYPE string,
             level           TYPE char01,
             syslog_class    TYPE val_text,
             syslog_category TYPE string,
             syslog_type     TYPE rslgmsgtyp,
             instance        TYPE instancex,
             wp_type         TYPE rslgwptype,
             processid       TYPE rslgselnum,
             client          TYPE mandt,
             zuser           TYPE rslguser,
             tcode           TYPE tcode,
             messageid       TYPE rslgno,
             text            TYPE rslg_text,
             terminal        TYPE rslg_terminal,
             repna           TYPE program_id,
             slgmode         TYPE sapmode,
           END OF result_type,

           BEGIN OF selector_type,
             messsageid_range TYPE cl_syslog_filter=>range_of_msgid,
             client_range     TYPE cl_syslog_filter=>range_of_mandt,
             user_range       TYPE cl_syslog_filter=>range_of_user,
             tcode_range      TYPE cl_syslog_filter=>range_of_tcode,
             wptype_range     TYPE cl_syslog_filter=>range_of_types,
             prog_range       TYPE cl_syslog_filter=>range_of_progids,
             pack_range       TYPE cl_syslog_filter=>range_of_devclass,
           END OF selector_type.


    CLASS-DATA: syslog_category TYPE SORTED TABLE OF syslog_category_type WITH UNIQUE KEY rslgnkky,
                syslog_class    TYPE SORTED TABLE OF syslog_class_type    WITH UNIQUE KEY domvalue_l,
                syslog_type     TYPE SORTED TABLE OF syslog_type_type     WITH UNIQUE KEY sltype.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt,

      resolve_level IMPORTING !monbew       TYPE rslgnkkd
                    RETURNING VALUE(result) TYPE char01,

      resolve_syslog_category IMPORTING !monkat       TYPE rslgnkkd
                              RETURNING VALUE(result) TYPE rslgnkvd,

      resolve_syslog_class IMPORTING !clasid       TYPE rslgclasid
                           RETURNING VALUE(result) TYPE val_text,

      resolve_syslog_type IMPORTING !slgftype     TYPE rslgtype_d
                          RETURNING VALUE(result) TYPE rslgmsgtyp.

    " set in constructor
    DATA: log          TYPE REF TO yddg__log,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE rslgentr_tab,

          " set in transform
          result_table TYPE TABLE OF result_type.


    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_sm21 IMPLEMENTATION.


  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'ASHOST'           val = row-instance        ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'WP_TYPE'          val = row-wp_type         ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'CLIENT'           val = row-client          ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'USER_ID'          val = row-zuser           ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TCODE'            val = row-tcode           ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'MESSAGEID'        val = row-messageid       ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'SYSLOG_CLASS'     val = row-syslog_class    ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'SYSLOG_CATEGORY'  val = row-syslog_category ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'TERMINAL'         val = row-terminal        ) TO tags.
    APPEND yddg__tags=>resolve( handle = 'REPORT'           val = row-repna           ) TO tags.

  ENDMETHOD.

  METHOD class_constructor.

    SELECT domvalue_l,
           ddtext
     FROM  dd07t
     WHERE domname = 'RSLGCLASID'
       AND ddlanguage = 'E'
     INTO
       TABLE @syslog_class.

    SELECT  rslgnkky,
            rslgnkva
      FROM  tsl4t
      WHERE rslgspra = 'E'
      INTO
        TABLE @syslog_category.

    SELECT  sltype,
            txt
      FROM  tsl3t
      WHERE spras = 'E'
      INTO
        TABLE @syslog_type.

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    selector-messsageid_range = messsageid_range.
    selector-client_range     = client_range.
    selector-user_range       = user_range.
    selector-tcode_range      = tcode_range.
    selector-wptype_range     = wptype_range.
    selector-prog_range       = prog_range.
    selector-pack_range       = pack_range.

    log = get_log( handle = 'SM21'  tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD resolve_level.

    CASE monbew+0(1).
      WHEN 'R'.    result = 'E'.
      WHEN 'Y'.    result = 'W'.
      WHEN 'G'.    result = 'S'.
      WHEN OTHERS. result = 'I'.
    ENDCASE.

  ENDMETHOD.


  METHOD resolve_syslog_category.

    DATA: entry TYPE syslog_category_type.

    READ TABLE syslog_category WITH TABLE KEY rslgnkky = monkat INTO entry.
    IF sy-subrc = 0.
      result = entry-rslgnkva.
    ENDIF.

  ENDMETHOD.


  METHOD resolve_syslog_class.

    DATA: entry TYPE syslog_class_type.

    READ TABLE syslog_class WITH TABLE KEY domvalue_l = clasid INTO entry.
    IF sy-subrc = 0.
      result = entry-ddtext.
    ENDIF.

  ENDMETHOD.


  METHOD resolve_syslog_type.

    DATA: entry TYPE syslog_type_type.

    READ TABLE syslog_type WITH TABLE KEY sltype = slgftype INTO entry.
    IF sy-subrc = 0.
      result = entry-txt.
    ENDIF.

  ENDMETHOD.


  METHOD extract.

    DATA: syslog_filter TYPE REF TO cl_syslog_filter.

    DATA(start_datetime) = yddg=>system( log->last_run + 1 ).
    DATA(end_datetime)    = yddg=>system( timestamp ).

    syslog_filter = NEW cl_syslog_filter( ).

    syslog_filter->set_filter_datetime( im_datetime_from = CONV #( start_datetime ) im_datetime_to = CONV #( end_datetime ) ).

    IF selector-messsageid_range IS NOT INITIAL.  syslog_filter->set_range_msgid(   selector-messsageid_range ).  ENDIF.
    IF selector-client_range     IS NOT INITIAL.  syslog_filter->set_range_mandt(   selector-client_range     ).  ENDIF.
    IF selector-user_range       IS NOT INITIAL.  syslog_filter->set_range_user(    selector-user_range       ).  ENDIF.
    IF selector-tcode_range      IS NOT INITIAL.  syslog_filter->set_range_tcode(   selector-tcode_range      ).  ENDIF.
    IF selector-wptype_range     IS NOT INITIAL.  syslog_filter->set_range_type(    selector-wptype_range     ).  ENDIF.
    IF selector-prog_range       IS NOT INITIAL.  syslog_filter->set_range_program( selector-prog_range       ).  ENDIF.
    IF selector-pack_range       IS NOT INITIAL.  syslog_filter->set_range_package( selector-pack_range       ).  ENDIF.

    syslog_filter->set_range_instance( VALUE #( ( sign = 'I'  option = 'CP'  low = '*' ) ) ).


    TRY.
        DATA(syslog) = cl_syslog=>get_instance_by_filter( syslog_filter ).
      CATCH cx_syslog_read_authorization. " Authorization to read syslog is missing
        MESSAGE e003 WITH 'YDDG_SM21'.
    ENDTRY.

    data_table = syslog->get_entries( ).

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<data_row>).

      MOVE-CORRESPONDING <data_row> TO result_row.
      result_row-utc = yddg=>iso( datetime_system = VALUE #( date = <data_row>-zdate
                                                             time = <data_row>-ztime ) ).
      result_row-level = resolve_level( <data_row>-monbew ).
      result_row-syslog_class = resolve_syslog_class( <data_row>-clasid ).
      result_row-syslog_category = resolve_syslog_category( <data_row>-monkat ).
      result_row-syslog_type = resolve_syslog_type( <data_row>-slgftype ).

      APPEND result_row to result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    TYPES: BEGIN OF line_type,
             utc       TYPE string,
             level     TYPE char01,
             class     TYPE val_text,
             category  TYPE string,
             type      TYPE rslgmsgtyp,
             instance  TYPE instancex,
             wp_type   TYPE rslgwptype,
             processid TYPE rslgselnum,
             client    TYPE mandt,
             user_id   TYPE rslguser,
             tcode     TYPE tcode,
             messageid TYPE rslgno,
             text      TYPE rslg_text,
             terminal  TYPE rslg_terminal,
             program   TYPE program_id,
             session   TYPE sapmode,
           END OF line_type,

           BEGIN OF json_type,
             class    TYPE val_text,
             category TYPE string,
             type     TYPE rslgmsgtyp,
             client   TYPE mandt,
             user_id  TYPE rslguser,
             tcode    TYPE tcode,
             terminal TYPE rslg_terminal,
             program  TYPE program_id,
             session  TYPE sapmode,
           END OF json_type.

    DATA: line   TYPE line_type,
          j_data TYPE json_type.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>).

      line = <r>.
      MOVE-CORRESPONDING line TO j_data.
      DATA(json) = /ui2/cl_json=>serialize( data = j_data  compress = abap_true  pretty_name = /ui2/cl_json=>pretty_mode-camel_case ).

      log->append( message = |[{ <r>-level } { <r>-utc } { <r>-instance } { <r>-wp_type }/{ <r>-processid }] { <r>-messageid }: { <r>-text }| && '||' && json
                   tags    = build_tags( <r> ) ).

    ENDLOOP.

    TRY.
        log->send( ).
      CATCH yddg_x_log_not_submitted INTO DATA(cx).
        timestamp = 0.
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    log->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
