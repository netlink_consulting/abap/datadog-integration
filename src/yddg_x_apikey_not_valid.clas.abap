CLASS yddg_x_apikey_not_valid DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: apikey TYPE yddg_apikey,
          host   TYPE yddg_cs_value.

    METHODS: constructor IMPORTING !apikey   TYPE yddg_apikey
                                   !host     TYPE yddg_cs_value
                                   !previous LIKE previous      OPTIONAL.

ENDCLASS.



CLASS yddg_x_apikey_not_valid IMPLEMENTATION.

  METHOD constructor  ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '903'
                        attr1    = 'APIKEY'
                        attr2    = 'HOST' ).

    me->apikey  = apikey.
    me->host = host.

  ENDMETHOD.

ENDCLASS.
