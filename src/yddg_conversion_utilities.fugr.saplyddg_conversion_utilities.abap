*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LYDDG_CONVERSION_UTILITIESTOP.              " Global Declarations
  INCLUDE LYDDG_CONVERSION_UTILITIESUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LYNLDD_CONVERSIONF...              " Subroutines
* INCLUDE LYNLDD_CONVERSIONO...              " PBO-Modules
* INCLUDE LYNLDD_CONVERSIONI...              " PAI-Modules
* INCLUDE LYNLDD_CONVERSIONE...              " Events
* INCLUDE LYNLDD_CONVERSIONP...              " Local class implement.
* INCLUDE LYNLDD_CONVERSIONT99.              " ABAP Unit tests
