CLASS yddg__tags DEFINITION PUBLIC CREATE PUBLIC .

  PUBLIC SECTION.

    CLASS-METHODS:

      class_constructor,

      resolve IMPORTING handle     TYPE yddg_handle
                        val        TYPE any
              RETURNING VALUE(tag) TYPE yddg_tag.

    DATA: tags TYPE yddg_tag_tt READ-ONLY.

    METHODS:

      constructor IMPORTING !tags  TYPE yddg_tag_tt OPTIONAL
                            !range TYPE yddg_tag_range_tt OPTIONAL
                              PREFERRED PARAMETER tags,

      add IMPORTING !tag   TYPE yddg_tag OPTIONAL
                    !tags  TYPE yddg_tag_tt OPTIONAL
                    !range TYPE yddg_tag_range_tt OPTIONAL
                      PREFERRED PARAMETER tag,

      union IMPORTING !tags         TYPE yddg_tag_tt OPTIONAL
                      !range        TYPE yddg_tag_range_tt OPTIONAL
                        PREFERRED PARAMETER tags
            RETURNING VALUE(result) TYPE yddg_tag_tt.

  PROTECTED SECTION.

    CLASS-DATA: tag_lookup TYPE SORTED TABLE OF yddg_tags WITH UNIQUE KEY handle.

    CLASS-METHODS: range_to_tags IMPORTING !range      TYPE yddg_tag_range_tt
                                 RETURNING VALUE(tags) TYPE yddg_tag_tt.

ENDCLASS.



CLASS yddg__tags IMPLEMENTATION.

  METHOD class_constructor.

    SELECT *
      FROM yddg_tags
      WHERE handle NOT LIKE 'SM12_%'
        AND handle NOT LIKE 'SM13_%'
      INTO TABLE @tag_lookup.

  ENDMETHOD.


  METHOD resolve.

    IF val IS NOT INITIAL.
      tag = |{ tag_lookup[ handle = handle ]-tag }:{ val }|.
    ENDIF.

  ENDMETHOD.


  METHOD add.

    IF range IS NOT INITIAL.
      IF tags IS NOT INITIAL OR tag IS NOT INITIAL.
        MESSAGE e001 WITH 'YNLDD_TAGS' 'ADD'.
      ENDIF.
      me->tags = union( range = range ).
    ELSEIF tags IS NOT INITIAL.
      IF  tag IS NOT INITIAL.
        MESSAGE e001 WITH 'YNLDD_TAGS' 'ADD'.
      ENDIF.
      me->tags = union( tags ).
    ELSEIF tag IS NOT INITIAL.
      me->tags = union( tags = VALUE #( ( tag ) ) ).
    ENDIF.

  ENDMETHOD.


  METHOD constructor.

    me->tags = yddg=>tags.
    me->tags = union( tags = tags  range = range ).

  ENDMETHOD.


  METHOD range_to_tags.

    tags = VALUE #( FOR r IN range WHERE ( sign = 'I' AND option = 'EQ' ) ( r-low ) ).

  ENDMETHOD.


  METHOD union.

    result = me->tags.

    IF range IS NOT INITIAL.
      IF tags IS NOT INITIAL.
        MESSAGE e001 WITH 'YNLDD_TAGS' 'UNION'.
      ENDIF.
      APPEND LINES OF range_to_tags( range ) TO result.
    ELSE.
      APPEND LINES OF tags TO result.
    ENDIF.

    SORT result.
    DELETE ADJACENT DUPLICATES FROM result.
    IF result[ 1 ] IS INITIAL.
      DELETE result INDEX 1.
    ENDIF.


  ENDMETHOD.

ENDCLASS.
