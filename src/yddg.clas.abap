CLASS yddg DEFINITION PUBLIC ABSTRACT FINAL CREATE PRIVATE.

  PUBLIC SECTION.

    TYPES: BEGIN OF configuration_type,
             api_key               TYPE yddg_apikey,
             api_host              TYPE yddg_cs_value,
             env                   TYPE yddg_cs_value,
             hostname              TYPE yddg_cs_value,
             log_host              TYPE yddg_cs_value,
             sid                   TYPE yddg_cs_value,
             submit_error_severity TYPE syst_msgty,
             lock_error_severity   TYPE syst_msgty,
           END OF configuration_type,

           instance_table_type TYPE TABLE OF msname2 WITH EMPTY KEY.

    CONSTANTS: epoch_start    TYPE d VALUE '19700101',
               seconds_in_day TYPE i VALUE 86400.

    CLASS-DATA: hostname              TYPE yddg_cs_value READ-ONLY,
                tags                  TYPE yddg_tag_tt READ-ONLY,
                submit_error_severity TYPE syst_msgty READ-ONLY,
                lock_error_severity   TYPE syst_msgty READ-ONLY,
                unit_tag              TYPE yddg_tag READ-ONLY.

    CLASS-METHODS:

      "! <p class="shorttext synchronized" lang="en">Covert Date/Time to ABAP Timestamp</p>
      "!
      "! @parameter datetime | <p class="shorttext synchronized" lang="en">Date/Time</p>
      "! @parameter abap_timestamp | <p class="shorttext synchronized" lang="en">ABAP Timestamp</p>
      abap_timestamp IMPORTING datetime              TYPE yddg_datetime
                     RETURNING VALUE(abap_timestamp) TYPE timestamp,

      "! <p class="shorttext synchronized" lang="en">Get https client for DD API</p>
      "!
      "! @parameter path | <p class="shorttext synchronized" lang="en">Path fragment</p>
      "! @parameter client | <p class="shorttext synchronized" lang="en">https client</p>
      "! @raising yddg_x_apihost_not_found | <p class="shorttext synchronized" lang="en">API Host not configured</p>
      "! @raising yddg_x_apikey_not_found | <p class="shorttext synchronized" lang="en">API Key not configured</p>
      "! @raising yddg_x_api_client | <p class="shorttext synchronized" lang="en">Internal SAP error</p>
      api_client IMPORTING path          TYPE string
                 RETURNING VALUE(client) TYPE REF TO if_http_client
                 RAISING   yddg_x_apihost_not_found
                           yddg_x_apikey_not_found
                           yddg_x_api_client,

      "! <p class="shorttext synchronized" lang="en">Load configuration and global tags</p>
      "!
      class_constructor,

      "! <p class="shorttext synchronized" lang="en">Get configuration data</p>
      "!
      "! @parameter config_data | <p class="shorttext synchronized" lang="en">Configuration data</p>
      configuration RETURNING        VALUE(config_data) TYPE configuration_type,

      "! <p class="shorttext synchronized" lang="en">Convert ABAP Timestamp to Date/Time</p>
      "!
      "! @parameter abap_timestamp | <p class="shorttext synchronized" lang="en">ABAP Timestamp</p>
      "! @parameter datetime | <p class="shorttext synchronized" lang="en">Date/Time</p>
      datetime IMPORTING abap_timestamp  TYPE timestamp
               RETURNING VALUE(datetime) TYPE yddg_datetime,

      instances RETURNING VALUE(result) TYPE instance_table_type,

      "! <p class="shorttext synchronized" lang="en">Convert to ISO Date/Time format: yyyy-mm-ddThh:mm:ssZ</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter utc_formatted | <p class="shorttext synchronized" lang="en">ISO formatted UTC</p>
      iso IMPORTING timestamp            TYPE yddg_ts OPTIONAL
                    utc                  TYPE yddg_datetime OPTIONAL
                    datetime_system      TYPE yddg_datetime OPTIONAL
                    datetime_user        TYPE yddg_datetime_user OPTIONAL
                    datetime_timezone    TYPE yddg_datetime_timezone OPTIONAL
                      PREFERRED PARAMETER timestamp
          RETURNING VALUE(utc_formatted) TYPE yddg_utc_formatted,

      "! <p class="shorttext synchronized" lang="en">Get https client for DD Log intake</p>
      "!
      "! @parameter client | <p class="shorttext synchronized" lang="en">https client</p>
      "! @raising yddg_x_apikey_not_found | <p class="shorttext synchronized" lang="en">API Key not configured</p>
      "! @raising yddg_x_api_client | <p class="shorttext synchronized" lang="en">Internal SAP error</p>
      "! @raising yddg_x_loghost_not_found | <p class="shorttext synchronized" lang="en">Log Host not configured</p>
      log_client RETURNING VALUE(client) TYPE REF TO if_http_client
                 RAISING   yddg_x_apikey_not_found
                           yddg_x_api_client
                           yddg_x_loghost_not_found,

      "! <p class="shorttext synchronized" lang="en">Return Timestamp with seconds set to zero</p>
      "!
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter ts | <p class="shorttext synchronized" lang="en">Tiumestamp with seconds set to zero</p>
      minute IMPORTING timestamp         TYPE yddg_ts OPTIONAL
                       utc               TYPE yddg_datetime OPTIONAL
                       datetime_system   TYPE yddg_datetime OPTIONAL
                       datetime_user     TYPE yddg_datetime_user OPTIONAL
                       datetime_timezone TYPE yddg_datetime_timezone OPTIONAL
                         PREFERRED PARAMETER timestamp
             RETURNING VALUE(ts)         TYPE yddg_ts,

      "! <p class="shorttext synchronized" lang="en">Return System Date and Time (default: now)</p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter datetime | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      system IMPORTING timestamp         TYPE yddg_ts OPTIONAL
                       utc               TYPE yddg_datetime OPTIONAL
                       datetime_user     TYPE yddg_datetime_user OPTIONAL
                       datetime_timezone TYPE yddg_datetime_timezone OPTIONAL
                         PREFERRED PARAMETER timestamp
             RETURNING VALUE(datetime)   TYPE yddg_datetime,

      "! <p class="shorttext synchronized" lang="en">Return Datadog Timestamp (epoch seconds) (default: now)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      timestamp IMPORTING utc               TYPE yddg_datetime OPTIONAL
                          datetime_system   TYPE yddg_datetime OPTIONAL
                          datetime_user     TYPE yddg_datetime_user OPTIONAL
                          datetime_timezone TYPE yddg_datetime_timezone OPTIONAL
                            PREFERRED PARAMETER utc
                RETURNING VALUE(timestamp)  TYPE yddg_ts,

      "! <p class="shorttext synchronized" lang="en">Return Date and Time with Timezone (default: now)</p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter timezone | <p class="shorttext synchronized" lang="en">Timezone (default: UTC)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      timezone IMPORTING timestamp                TYPE yddg_ts OPTIONAL
                         timezone                 TYPE tznzone DEFAULT 'UTC'
                         utc                      TYPE yddg_datetime OPTIONAL
                         datetime_system          TYPE yddg_datetime OPTIONAL
                         datetime_user            TYPE yddg_datetime_user OPTIONAL
                           PREFERRED PARAMETER timestamp
               RETURNING VALUE(datetime_timezone) TYPE yddg_datetime_timezone,

      "! <p class="shorttext synchronized" lang="en">Return User Date and Time  (default: now)</p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter user_id | <p class="shorttext synchronized" lang="en">User (default: current)</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      user IMPORTING timestamp            TYPE yddg_ts OPTIONAL
                     user_id              TYPE xubname DEFAULT sy-uname
                     utc                  TYPE yddg_datetime OPTIONAL
                     datetime_system      TYPE yddg_datetime OPTIONAL
                     datetime_timezone    TYPE yddg_datetime_timezone OPTIONAL
                       PREFERRED PARAMETER timestamp
           RETURNING VALUE(datetime_user) TYPE yddg_datetime_user,

      "! <p class="shorttext synchronized" lang="en">Return UTC Date/Time (default: now)</p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en">Datadog Timestamp (epoch seconds)</p>
      "! @parameter datetime_system | <p class="shorttext synchronized" lang="en">System Date/Time</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en">User Date/Time</p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en">Date/Time with Timezone</p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en">UTC Date/Time</p>
      utc IMPORTING timestamp         TYPE yddg_ts OPTIONAL
                    datetime_system   TYPE yddg_datetime OPTIONAL
                    datetime_user     TYPE yddg_datetime_user OPTIONAL
                    datetime_timezone TYPE yddg_datetime_timezone OPTIONAL
                      PREFERRED PARAMETER timestamp
          RETURNING VALUE(utc)        TYPE yddg_datetime,

      "! <p class="shorttext synchronized" lang="en">Connect to API Host and Check</p>
      "!
      validate_api_key,

      "! <p class="shorttext synchronized" lang="en">Display current version</p>
      "!
      version,

      "! <p class="shorttext synchronized" lang="en">Dump JSON to output (spool)</p>
      "!
      "! @parameter json | <p class="shorttext synchronized" lang="en">JSON Data</p>
      write_json IMPORTING json TYPE string.

  PROTECTED SECTION.

    TYPES: BEGIN OF timezone_buffer_type,
             name     TYPE xubname,
             timezone TYPE tznzone,
           END OF timezone_buffer_type.

    CLASS-DATA: api_host              TYPE yddg_cs_value,
                api_key               TYPE yddg_apikey,
                env                   TYPE yddg_cs_value,
                log_host              TYPE yddg_cs_value,
                system_timezone_value TYPE tznzone,

                timezone_buffer       TYPE HASHED TABLE OF timezone_buffer_type WITH UNIQUE KEY name.

    CLASS-METHODS:

      "! <p class="shorttext synchronized" lang="en">Get Client to send data to Datadog</p>
      "!
      "! @parameter host | <p class="shorttext synchronized" lang="en">Host (either api or log)</p>
      "! @parameter path | <p class="shorttext synchronized" lang="en">sub path</p>
      "! @parameter client | <p class="shorttext synchronized" lang="en">IF_HTTP_CLIENT instance</p>
      "! @raising yddg_x_apikey_not_found | <p class="shorttext synchronized" lang="en"></p>
      "! @raising yddg_x_api_client | <p class="shorttext synchronized" lang="en">Unspecified error</p>
      https_client IMPORTING host          TYPE yddg_cs_value
                             path          TYPE string
                   RETURNING VALUE(client) TYPE REF TO if_http_client
                   RAISING   yddg_x_apikey_not_found
                             yddg_x_api_client,

      "! <p class="shorttext synchronized" lang="en">Timezone key of system (buffered)</p>
      "!
      "! @parameter timezone | <p class="shorttext synchronized" lang="en"></p>
      system_timezone RETURNING VALUE(timezone) TYPE tznzone,

      "! <p class="shorttext synchronized" lang="en">Timezone of User from USR02</p>
      "!
      "! @parameter user_id | <p class="shorttext synchronized" lang="en">defaults to current user</p>
      "! @parameter timezone | <p class="shorttext synchronized" lang="en"></p>
      user_timezone IMPORTING user_id         TYPE xubname
                    RETURNING VALUE(timezone) TYPE tznzone,

      "! <p class="shorttext synchronized" lang="en">Return ISO formatted string (yyyy-mm-ddThh:mm:ssZ)</p>
      "!
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter utc_formatted | <p class="shorttext synchronized" lang="en"></p>
      utc_to_iso IMPORTING utc                  TYPE yddg_datetime
                 RETURNING VALUE(utc_formatted) TYPE yddg_utc_formatted,

      "! <p class="shorttext synchronized" lang="en">Return system date/time</p>
      "!
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter datetime | <p class="shorttext synchronized" lang="en"></p>
      utc_to_system IMPORTING utc             TYPE yddg_datetime
                    RETURNING VALUE(datetime) TYPE yddg_datetime,

      "! <p class="shorttext synchronized" lang="en">Return Datadog timestamp (epoch)</p>
      "!
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter timestamp | <p class="shorttext synchronized" lang="en"></p>
      utc_to_timestamp IMPORTING utc              TYPE yddg_datetime
                       RETURNING VALUE(timestamp) TYPE yddg_ts,

      "! <p class="shorttext synchronized" lang="en">Return User date/time</p>
      "!
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter user_id | <p class="shorttext synchronized" lang="en">defaults to current user</p>
      "! @parameter datetime_user | <p class="shorttext synchronized" lang="en"></p>
      utc_to_user IMPORTING utc                  TYPE yddg_datetime
                            user_id              TYPE xubname
                  RETURNING VALUE(datetime_user) TYPE yddg_datetime_user,

      "! <p class="shorttext synchronized" lang="en">Return date/time for specified timezone</p>
      "!
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter timezone | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en"></p>
      utc_to_zone IMPORTING utc                      TYPE yddg_datetime
                            timezone                 TYPE tznzone
                  RETURNING VALUE(datetime_timezone) TYPE yddg_datetime_timezone,

      "! <p class="shorttext synchronized" lang="en">Return UTC from Timezone date/time</p>
      "!
      "! @parameter datetime_timezone | <p class="shorttext synchronized" lang="en"></p>
      "! @parameter utc | <p class="shorttext synchronized" lang="en"></p>
      zone_to_utc      IMPORTING datetime_timezone TYPE yddg_datetime_timezone
                       RETURNING VALUE(utc)        TYPE yddg_datetime.

ENDCLASS.



CLASS yddg IMPLEMENTATION.


  METHOD abap_timestamp.

    CONVERT DATE datetime-date TIME datetime-time INTO TIME STAMP abap_timestamp TIME ZONE 'UTC'.

  ENDMETHOD.


  METHOD api_client.

    IF api_host IS INITIAL.
      RAISE EXCEPTION TYPE yddg_x_apihost_not_found.
    ENDIF.
    client = https_client( host = api_host  path = path ).

  ENDMETHOD.


  METHOD class_constructor.

    DATA(config_data) = configuration( ).

    api_key               = config_data-api_key.
    api_host              = config_data-api_host.
    log_host              = config_data-log_host.
    env                   = config_data-env.
    hostname              = config_data-hostname.
    submit_error_severity = config_data-submit_error_severity.
    lock_error_severity   = config_data-lock_error_severity.

    SELECT tag
      FROM yddg_global_tags
      INTO TABLE @tags.

    IF env IS NOT INITIAL.
      APPEND |env:{ env }| TO tags.
    ENDIF.

    APPEND |sap_sid:{ config_data-sid }| TO tags.

    SELECT mandt,
           cccategory
      FROM t000
      INTO TABLE @DATA(t000).

    unit_tag = |other_unit:{ config_data-sid }|.
    " get max client category of system
    LOOP AT t000 ASSIGNING FIELD-SYMBOL(<t000>).
      IF <t000>-cccategory = 'P'.
        unit_tag = |prd_unit:{ config_data-sid }|.
      ELSEIF <t000>-cccategory = 'T'.
        unit_tag = |qa_unit:{ config_data-sid }|.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.


  METHOD configuration.

    config_data-submit_error_severity = 'W'.
    config_data-lock_error_severity = 'W'.
    config_data-sid = sy-sysid.

    SELECT *
      FROM yddg_config
      INTO TABLE @DATA(rows).

    LOOP AT rows ASSIGNING FIELD-SYMBOL(<row>).
      CASE <row>-handle.
        WHEN 'API_KEY'.               config_data-api_key               = <row>-value.
        WHEN 'API_HOST'.              config_data-api_host              = <row>-value.
        WHEN 'ENV'.                   config_data-env                   = <row>-value.
        WHEN 'HOSTNAME'.              config_data-hostname              = <row>-value.
        WHEN 'LOG_HOST'.              config_data-log_host              = <row>-value.
        WHEN 'SID'.                   config_data-sid                   = <row>-value.
        WHEN 'SUBMIT_ERROR_SEVERITY'. config_data-submit_error_severity = <row>-value.
        WHEN 'LOCK_ERROR_SEVERITY'.   config_data-lock_error_severity   = <row>-value.
      ENDCASE.
    ENDLOOP.

  ENDMETHOD.


  METHOD datetime.

    CONVERT TIME STAMP abap_timestamp TIME ZONE 'UTC' INTO DATE datetime-date TIME datetime-time.

  ENDMETHOD.


  METHOD https_client.

    IF api_key IS INITIAL.
      RAISE EXCEPTION TYPE yddg_x_apikey_not_found.
    ENDIF.

    cl_http_client=>create_by_url( EXPORTING  url                = 'https://' && host && '/api/' && path
                                   IMPORTING  client             = client
                                   EXCEPTIONS argument_not_found = 1
                                              plugin_not_active  = 2
                                              internal_error     = 3
                                              OTHERS             = 4 ).
    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE yddg_x_api_client
        EXPORTING
          host = host
          path = path.
    ENDIF.

    client->request->set_header_field( name = 'Accept'        value = 'application/json' ).
    client->request->set_header_field( name = 'Content-Type'  value = 'application/json' ).
    client->request->set_header_field( name = 'DD-API-KEY'    value = CONV #( api_key ) ).

  ENDMETHOD.


  METHOD iso.

    IF utc IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL OR
         datetime_system   IS NOT INITIAL OR
         timestamp         IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'ISO'.
      ENDIF.
      utc_formatted = utc_to_iso( utc ).
    ELSE.
      utc_formatted = utc_to_iso( utc( timestamp         = timestamp
                                       datetime_system   = datetime_system
                                       datetime_user     = datetime_user
                                       datetime_timezone = datetime_timezone ) ).
    ENDIF.

  ENDMETHOD.


  METHOD minute.

    DATA: abap_ts_c TYPE c LENGTH 14.

    IF utc IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL OR
         datetime_system   IS NOT INITIAL OR
         timestamp         IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'ISO'.
      ENDIF.
      DATA(abap_ts) = abap_timestamp( utc ).
    ELSE.
      abap_ts = abap_timestamp( utc( timestamp         = timestamp
                                     datetime_system   = datetime_system
                                     datetime_user     = datetime_user
                                     datetime_timezone = datetime_timezone ) ).
    ENDIF.

    abap_ts_c = abap_ts.
    abap_ts_c+12(2) = '00'.
    abap_ts = abap_ts_c.

    ts = timestamp( datetime( abap_ts ) ).

  ENDMETHOD.


  METHOD log_client.

    IF log_host IS INITIAL.
      RAISE EXCEPTION TYPE yddg_x_loghost_not_found.
    ENDIF.

    client = https_client( host = log_host  path = 'v2/logs' ).

  ENDMETHOD.


  METHOD system.

    IF utc IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL OR
         timestamp         IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'SYSTEM'.
      ENDIF.
      datetime = utc_to_system( utc ).
    ELSE.
      datetime = utc_to_system( utc( timestamp         = timestamp
                                     datetime_user     = datetime_user
                                     datetime_timezone = datetime_timezone ) ).
    ENDIF.

  ENDMETHOD.


  METHOD system_timezone.

    IF system_timezone_value IS INITIAL.
      SELECT SINGLE tzonesys FROM ttzcu INTO @system_timezone_value.
      IF sy-subrc <> 0.
        SELECT SINGLE tzonesys FROM ttzcu CLIENT SPECIFIED WHERE client = '000' INTO @system_timezone_value.
        IF sy-subrc <> 0.
          system_timezone_value = 'UTC'.
        ENDIF.
      ENDIF.
    ENDIF.

    timezone = system_timezone_value.

  ENDMETHOD.


  METHOD timestamp.

    IF utc IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL OR
         datetime_system   IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'TIMESTAMP'.
      ENDIF.
      timestamp = utc_to_timestamp( utc ).
    ELSE.
      timestamp = utc_to_timestamp( utc( datetime_system   = datetime_system
                                         datetime_user     = datetime_user
                                         datetime_timezone = datetime_timezone ) ).
    ENDIF.

  ENDMETHOD.


  METHOD timezone.

    IF utc IS NOT INITIAL.
      IF datetime_user   IS NOT INITIAL OR
         datetime_system IS NOT INITIAL OR
         timestamp       IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'TIMEZONE'.
      ENDIF.
      datetime_timezone = utc_to_zone( utc = utc  timezone = timezone ).
    ELSE.
      datetime_timezone = utc_to_zone( utc = utc( timestamp       = timestamp
                                                  datetime_system = datetime_system
                                                  datetime_user   = datetime_user )
                                 timezone = timezone ).
    ENDIF.

  ENDMETHOD.


  METHOD user.

    IF utc IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_system   IS NOT INITIAL OR
         timestamp         IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'USER'.
      ENDIF.
      datetime_user = utc_to_user( utc = utc  user_id = user_id ).
    ELSE.
      datetime_user = utc_to_user( utc = utc( timestamp         = timestamp
                                              datetime_system   = datetime_system
                                              datetime_timezone = datetime_timezone )
                                   user_id = user_id ).
    ENDIF.

  ENDMETHOD.


  METHOD user_timezone.

    IF user_id = sy-uname.
      timezone = sy-zonlo.

    ELSE.
      READ TABLE timezone_buffer WITH TABLE KEY name = user_id INTO DATA(buffer).
      IF sy-subrc <> 0.
        SELECT SINGLE tzone FROM usr02 WHERE bname = @user_id INTO @buffer-timezone.
        IF sy-subrc <> 0.
          buffer-timezone = system_timezone( ).
        ENDIF.
        buffer-name = user_id.
        INSERT buffer INTO TABLE timezone_buffer.
      ENDIF.
      timezone = buffer-timezone.
    ENDIF.

  ENDMETHOD.


  METHOD utc.

    IF timestamp IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL OR
         datetime_system   IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'UTC'.
      ENDIF.
      utc-date = timestamp DIV seconds_in_day  + epoch_start.
      utc-time = timestamp MOD seconds_in_day.

    ELSEIF datetime_system IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL OR
         datetime_user     IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'UTC'.
      ENDIF.
      utc = zone_to_utc( VALUE #( date = datetime_system-date
                                  time     = datetime_system-time
                                  timezone = system_timezone( ) ) ).

    ELSEIF datetime_user IS NOT INITIAL.
      IF datetime_timezone IS NOT INITIAL.
        MESSAGE e014(ynlddb) WITH 'UTC'.
      ENDIF.
      utc = zone_to_utc( VALUE #( date     = datetime_user-date
                                  time     = datetime_user-time
                                  timezone = user_timezone( datetime_user-user_id ) ) ).

    ELSEIF datetime_timezone IS NOT INITIAL.
      utc = zone_to_utc( datetime_timezone ).

    ELSE.
      utc = zone_to_utc( VALUE #( date     = sy-datum
                                  time     = sy-uzeit
                                  timezone = sy-zonlo ) ).
    ENDIF.

  ENDMETHOD.


  METHOD utc_to_iso.

    utc_formatted = |{ utc-date DATE = ISO }T{ utc-time TIME = ISO }Z|.

  ENDMETHOD.


  METHOD utc_to_system.

    datetime = utc_to_zone( utc = utc  timezone = system_timezone( ) ).

  ENDMETHOD.


  METHOD utc_to_timestamp.

    timestamp = CONV i( utc-date - epoch_start ) * seconds_in_day + CONV i( utc-time ).

  ENDMETHOD.


  METHOD utc_to_user.

    datetime_user = utc_to_zone( utc = utc  timezone = user_timezone( user_id  ) ).
    datetime_user-user_id = user_id.

  ENDMETHOD.


  METHOD utc_to_zone.

    CONVERT TIME STAMP abap_timestamp( utc )
                 TIME ZONE timezone
            INTO DATE datetime_timezone-date
                 TIME datetime_timezone-time.
    datetime_timezone-timezone = timezone.

  ENDMETHOD.


  METHOD validate_api_key.

    DATA: http_status TYPE i,
          except      TYPE REF TO yddg_x.

    TRY.
        DATA(client) = api_client( path = 'v1/validate' ).
        client->send( timeout = if_http_client=>co_timeout_default ).
        client->receive( ).
        client->response->get_status( IMPORTING code = http_status ).
        client->close( ).
        IF http_status <> 200.
          MESSAGE i903 DISPLAY LIKE 'E'
                               WITH api_key
                                    api_host.
        ELSE.
          MESSAGE i000 DISPLAY LIKE 'S'
                               WITH 'API key is valid'.
        ENDIF.
      CATCH yddg_x INTO except.
        MESSAGE i000 DISPLAY LIKE 'E'
                             WITH CONV syst_msgv( except->get_text( ) ).
    ENDTRY.

  ENDMETHOD.


  METHOD version.

    MESSAGE i000 DISPLAY LIKE 'S'
                 WITH 'Datadog Integration by NetLink - Version:'
                      CONV syst_msgv( TEXT-v00 ).

  ENDMETHOD.


  METHOD zone_to_utc.

    DATA: abap_timestamp TYPE timestamp.

    CONVERT DATE datetime_timezone-date
            TIME datetime_timezone-time
            INTO TIME STAMP abap_timestamp
                 TIME ZONE  datetime_timezone-timezone.

    utc = datetime( abap_timestamp ).

  ENDMETHOD.


  METHOD write_json.

    DATA: lines TYPE TABLE OF string.

    TRY.
        DATA(json_xstring) = cl_abap_codepage=>convert_to( json ).
        DATA(reader)       = cl_sxml_string_reader=>create( json_xstring ).
        DATA(writer)       = CAST if_sxml_writer( cl_sxml_string_writer=>create( type = if_sxml=>co_xt_json ) ).
        writer->set_option( option = if_sxml_writer=>co_opt_linebreaks ).
        writer->set_option( option = if_sxml_writer=>co_opt_indent ).
        reader->next_node( ).
        reader->skip_node( writer ).
        DATA(json_formatted_string) = cl_abap_codepage=>convert_from( CAST cl_sxml_string_writer( writer )->get_output( ) ).
        SPLIT json_formatted_string AT cl_abap_char_utilities=>newline INTO TABLE lines.

        SKIP 2.
        WRITE: / 'JSON:'.
        SKIP 1.
        LOOP AT lines ASSIGNING FIELD-SYMBOL(<line>).
          WRITE: / <line>.
        ENDLOOP.
      CATCH cx_sxml_parse_error.
        WRITE: / 'Cannot parse JSON.'.
    ENDTRY.

  ENDMETHOD.

  METHOD instances.

    DATA: list TYPE TABLE OF msxxlist.

    CALL FUNCTION 'TH_SERVER_LIST'
      TABLES
        list = list.

    LOOP AT list ASSIGNING FIELD-SYMBOL(<l>).
      APPEND <l>-name TO result.
    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
