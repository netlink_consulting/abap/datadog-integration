CLASS yddg__p DEFINITION PUBLIC ABSTRACT CREATE PROTECTED.

  PUBLIC SECTION.

    METHODS: run.

  PROTECTED SECTION.

    DATA: tags         TYPE REF TO yddg__tags,
          trace_active TYPE abap_bool,
          timestamp    TYPE yddg_ts.

    METHODS:

      constructor IMPORTING !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                            !trace_active TYPE abap_bool,

      extract    ABSTRACT,
      transform  ABSTRACT,
      load       ABSTRACT,
      finalize   ABSTRACT,

      get_metric IMPORTING !handle       TYPE yddg_metric
                           !tags         TYPE REF TO yddg__tags   OPTIONAL
                           !trace_active TYPE abap_bool           DEFAULT abap_false
                           !package_size TYPE i                   DEFAULT 100
                 RETURNING VALUE(metric) TYPE REF TO yddg__metric,

      get_log IMPORTING !handle       TYPE yddg_metric
                        !tags         TYPE REF TO yddg__tags   OPTIONAL
                        !trace_active TYPE abap_bool           DEFAULT abap_false
                        !package_size TYPE i                   DEFAULT 50
              RETURNING VALUE(metric) TYPE REF TO yddg__log,

      get_event IMPORTING !handle       TYPE yddg_metric
                        !tags         TYPE REF TO yddg__tags   OPTIONAL
                        !trace_active TYPE abap_bool           DEFAULT abap_false
                        !package_size TYPE i                   DEFAULT 50
              RETURNING VALUE(metric) TYPE REF TO yddg__event,


      write_table IMPORTING !name TYPE string
                            !tab  TYPE ANY TABLE.

  PRIVATE SECTION.

    TYPES: BEGIN OF data_type,
             dummy TYPE i,
           END OF data_type,

           BEGIN OF result_type,
             dummy TYPE i,
           END OF result_type.

    DATA: data_table   TYPE TABLE OF data_tab,
          result_table TYPE TABLE OF result_type.

ENDCLASS.



CLASS yddg__p IMPLEMENTATION.

  METHOD constructor.

    me->tags         = NEW #( range = tag_range ).
    me->trace_active = trace_active.
    me->timestamp    = yddg=>timestamp( ).

  ENDMETHOD.


  METHOD get_metric.

    metric = NEW #( handle       = handle
                    tags         = tags
                    trace_active = trace_active
                    package_size = package_size ).


    CALL FUNCTION 'ENQUEUE_EYDDG_METRIC'
      EXPORTING
        handle         = handle
      EXCEPTIONS
        foreign_lock   = 1                " Object already locked
        system_failure = 2                " Internal error from enqueue server
        OTHERS         = 3.
    IF sy-subrc <> 0.
      DATA(cx) = NEW yddg_x_cannot_lock_metric( user   = CONV #( sy-msgv1 )
                                                metric = handle ).
      MESSAGE cx TYPE yddg=>lock_error_severity DISPLAY LIKE 'E'.
      LEAVE PROGRAM.
    ENDIF.

  ENDMETHOD.


  METHOD get_log.

    metric = NEW #( handle       = handle
                    tags         = tags
                    trace_active = trace_active
                    package_size = package_size ).

    CALL FUNCTION 'ENQUEUE_EYDDG_METRIC'
      EXPORTING
        handle         = handle
      EXCEPTIONS
        foreign_lock   = 1                " Object already locked
        system_failure = 2                " Internal error from enqueue server
        OTHERS         = 3.
    IF sy-subrc <> 0.
      DATA(cx) = NEW yddg_x_cannot_lock_metric( user   = CONV #( sy-msgv1 )
                                                metric = handle ).
      MESSAGE cx TYPE yddg=>lock_error_severity DISPLAY LIKE 'E'.
      LEAVE PROGRAM.
    ENDIF.

  ENDMETHOD.


  METHOD get_event.

    metric = NEW #( handle       = handle
                    tags         = tags
                    trace_active = trace_active
                    package_size = package_size ).

    CALL FUNCTION 'ENQUEUE_EYDDG_METRIC'
      EXPORTING
        handle         = handle
      EXCEPTIONS
        foreign_lock   = 1                " Object already locked
        system_failure = 2                " Internal error from enqueue server
        OTHERS         = 3.
    IF sy-subrc <> 0.
      DATA(cx) = NEW yddg_x_cannot_lock_metric( user   = CONV #( sy-msgv1 )
                                                metric = handle ).
      MESSAGE cx TYPE yddg=>lock_error_severity DISPLAY LIKE 'E'.
      LEAVE PROGRAM.
    ENDIF.

  ENDMETHOD.

  METHOD run.

    extract( ).
    transform( ).
    load( ).
    finalize( ).

  ENDMETHOD.


  METHOD write_table.

    TYPES: BEGIN OF format_type,
             col_name   TYPE c LENGTH 30,
             out_length TYPE i,
           END OF format_type.

    DATA: format_table TYPE TABLE OF format_type.

    DATA: o_data   TYPE REF TO data,
          o_struct TYPE REF TO cl_abap_structdescr,
          o_table  TYPE REF TO cl_abap_tabledescr.

    FIELD-SYMBOLS: <f_data> TYPE table.

    IF trace_active = abap_true.

      GET REFERENCE OF tab INTO o_data.
      ASSIGN o_data->* TO <f_data>.

      o_table  ?= cl_abap_structdescr=>describe_by_data_ref( o_data ).
      o_struct ?= o_table->get_table_line_type( ).

      format_table = VALUE #( FOR d IN o_struct->components
                                LET l = nmin( val1 = 50
                                              val2 = nmax( val1 = strlen( d-name )
                                                           val2 = SWITCH i( d-type_kind WHEN 'C' THEN d-length / 2
                                                                                        WHEN 'I' THEN 21
                                                                                        WHEN 'X' THEN d-length * 2
                                                                                        ELSE  d-length ) ) ) IN
                                    ( col_name   = d-name
                                      out_length = l ) ).

      DATA(total_line_length) = 1.
      LOOP AT format_table ASSIGNING FIELD-SYMBOL(<format>).
        ADD <format>-out_length TO total_line_length.
        ADD 1 TO total_line_length.
      ENDLOOP.

      DATA(separator) = repeat( val = '-'  occ = total_line_length ).
      SKIP 1.

      WRITE: / separator.

      WRITE: / '| itab:', name, AT total_line_length '|'.
      WRITE: / separator.
      WRITE: / '|' NO-GAP.
      LOOP AT format_table ASSIGNING <format>.
        WRITE: AT (<format>-out_length) |{ <format>-col_name CASE = LOWER }| NO-GAP, '|' NO-GAP.
      ENDLOOP.
      WRITE: / separator.


      FIELD-SYMBOLS: <col> TYPE any.

      LOOP AT <f_data> ASSIGNING FIELD-SYMBOL(<row>).
        WRITE: / '|' NO-GAP.
        LOOP AT format_table ASSIGNING <format>.
          ASSIGN COMPONENT <format>-col_name OF STRUCTURE <row> TO <col>.
          WRITE: AT (<format>-out_length) <col> NO-GAP, '|' NO-GAP.
        ENDLOOP.
      ENDLOOP.
      WRITE: / separator.

    ENDIF.

  ENDMETHOD.

ENDCLASS.
