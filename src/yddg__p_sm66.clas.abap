CLASS yddg__p_sm66 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS: constructor IMPORTING !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             wp_typ     TYPE wptyp,
             wp_status  TYPE wpstatus,
             wp_waiting TYPE wpwaiting,
             wp_mandt   TYPE mandt,
             wp_bname   TYPE wpbname,
             wp_report  TYPE wpreport,
             wp_server  TYPE msname,
             wp_waitinf TYPE wpwaitinf,
             tcode      TYPE sta_tcode4,
           END OF data_type,

           result_type TYPE yddg_sm66_rb.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: metric       TYPE REF TO yddg__metric,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_sm66 IMPLEMENTATION.


  METHOD build_tags.

    APPEND yddg__tags=>resolve( handle = 'ASHOST'       val =  row-wp_server  ) TO tags.  " sap_instance
    APPEND yddg__tags=>resolve( handle = 'WP_TYPE'      val =  row-wp_typ     ) TO tags.  " sap_wp_type
    APPEND yddg__tags=>resolve( handle = 'WP_STATUS'    val =  row-wp_status  ) TO tags.  " sap_wp_status
    APPEND yddg__tags=>resolve( handle = 'WP_HOLD'      val =  row-wp_waiting ) TO tags.  " sap_wp_hold
    APPEND yddg__tags=>resolve( handle = 'WP_HOLD_INFO' val =  row-wp_waitinf ) TO tags.  " sap_wp_hold_info
    APPEND yddg__tags=>resolve( handle = 'CLIENT'       val =  row-wp_mandt   ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'USER_ID'      val =  row-wp_bname   ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'TCODE'        val =  row-tcode      ) TO tags.  " sap_tcode
    APPEND yddg__tags=>resolve( handle = 'REPORT'       val =  row-wp_report  ) TO tags.  " sap_program

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    metric = get_metric( handle = 'SM66'  tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD extract.

    DATA: wptotlinfo TYPE STANDARD TABLE OF wptotlinfo,
          comerrs    TYPE STANDARD TABLE OF wptotlicer,
          used_wp    TYPE STANDARD TABLE OF wptotliuwp.

    PERFORM get_data
         IN PROGRAM sapmsm66
           TABLES wptotlinfo
                  comerrs
                  used_wp
           USING  space
                  space
                  space
                  'X'.

    MOVE-CORRESPONDING wptotlinfo TO data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          wp_typ     = <r>-wp_typ
          wp_status  = <r>-wp_status
          wp_waiting = <r>-wp_waiting
          wp_mandt   = <r>-wp_mandt
          wp_bname   = <r>-wp_bname
          wp_report  = <r>-wp_report
          wp_server  = <r>-wp_server
          wp_waitinf = <r>-wp_waitinf
          tcode      = <r>-tcode
          value      = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          wp_typ     = row->wp_typ
          wp_status  = row->wp_status
          wp_waiting = row->wp_waiting
          wp_mandt   = row->wp_mandt
          wp_bname   = row->wp_bname
          wp_report  = row->wp_report
          wp_server  = row->wp_server
          wp_waitinf = row->wp_waitinf
          tcode      = row->tcode
          value      = row->value ) TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT wp_typ,
           wp_status,
           wp_waiting,
           wp_mandt,
           wp_bname,
           wp_report,
           wp_server,
           wp_waitinf,
           tcode
      FROM yddg_sm66_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          wp_typ     = <r>-wp_typ
          wp_status  = <r>-wp_status
          wp_waiting = <r>-wp_waiting
          wp_mandt   = <r>-wp_mandt
          wp_bname   = <r>-wp_bname
          wp_report  = <r>-wp_report
          wp_server  = <r>-wp_server
          wp_waitinf = <r>-wp_waitinf
          tcode      = <r>-tcode
          ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_sm66_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_sm66_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
