CLASS yddg__p_bd87 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: client_range_type       TYPE RANGE OF mandt,
           status_range_type       TYPE RANGE OF edi_status,
           partner_range_type      TYPE RANGE OF sendsystem,
           message_type_range_type TYPE RANGE OF edi_mestyp.

    METHODS: constructor IMPORTING !client_range       TYPE client_range_type       OPTIONAL
                                   !outbound           TYPE abap_bool               DEFAULT abap_true
                                   !inbound            TYPE abap_bool               DEFAULT abap_true
                                   !status_range       TYPE status_range_type       OPTIONAL
                                   !partner_range      TYPE partner_range_type      OPTIONAL
                                   !message_type_range TYPE message_type_range_type OPTIONAL
                                   !bracket_set        TYPE yddg_bracket DEFAULT 'AGE'
                                   !other_tag          TYPE yddg_tag DEFAULT 'sap_age:other'
                                   !tag_range          TYPE yddg_tag_range_tt       OPTIONAL
                                   !trace_active       TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             mandt  TYPE mandt,
             status TYPE edi_status,
             direct TYPE edi_direct,
             rcvprn TYPE edi_rcvprn,
             sndprn TYPE edi_sndprn,
             credat TYPE edi_ccrdat,
             cretim TYPE edi_ccrtim,
             mestyp TYPE edi_mestyp,
             idoctp TYPE  edi_idoctp,
           END OF data_type,

           result_type TYPE yddg_bd87_rb,

           BEGIN OF selector_type,
             client_range       TYPE client_range_type,
             outbound           TYPE abap_bool,
             inbound            TYPE abap_bool,
             status_range       TYPE status_range_type,
             partner_range      TYPE partner_range_type,
             message_type_range TYPE message_type_range_type,
           END OF selector_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.

    " set in constructor
    DATA: bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric_out   TYPE REF TO yddg__metric,
          metric_in    TYPE REF TO yddg__metric,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_bd87 IMPLEMENTATION.

  METHOD build_tags.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'             val = row-mandt  ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'IDOC_STATUS'        val = row-status ) TO tags.  " sap_idoc_status
    APPEND yddg__tags=>resolve( handle = 'PARTNER_RECEIVING'  val = row-rcvprn ) TO tags.  " sap_partner_receiving
    APPEND yddg__tags=>resolve( handle = 'PARTNER_SENDING'    val = row-sndprn ) TO tags.  " sap_partner_sending
    APPEND yddg__tags=>resolve( handle = 'IDOC_MSG_TYPE'      val = row-mestyp ) TO tags.  " sap_idoc_message_type
    APPEND yddg__tags=>resolve( handle = 'IDOC_TYPE'          val = row-idoctp ) TO tags.  " sap_idoc_message_type

  ENDMETHOD.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set  = bracket_set.
    me->other_tag    = other_tag.

    metric_out = get_metric( handle = 'BD87_OUT'  tags  = tags  trace_active = trace_active ).
    metric_in  = get_metric( handle = 'BD87_IN'   tags  = tags  trace_active = trace_active ).

    me->selector-client_range       = client_range.
    me->selector-outbound           = outbound.
    me->selector-inbound            = inbound.
    me->selector-status_range       = status_range.
    me->selector-partner_range      = partner_range.
    me->selector-message_type_range = message_type_range.

  ENDMETHOD.


  METHOD extract.

    DATA: direction_range TYPE RANGE OF edi_direct.

    IF selector-outbound = abap_true.
      APPEND VALUE #( sign = 'I'  option = 'EQ'  low = '1' ) TO direction_range.
    ENDIF.
    IF selector-inbound = abap_true.
      APPEND VALUE #( sign = 'I'  option = 'EQ'  low = '2' ) TO direction_range.
    ENDIF.

    SELECT mandt,
           status,
           direct,
           rcvprn,
           sndprn,
           credat,
           cretim,
           mestyp,
           idoctp
      FROM edidc CLIENT SPECIFIED
      WHERE mandt IN @selector-client_range
        AND status IN @selector-status_range
        AND direct IN @direction_range
        AND mestyp IN @selector-message_type_range
        AND ( rcvprn IN @selector-partner_range OR sndprn IN @selector-partner_range )
      INTO CORRESPONDING FIELDS OF TABLE @data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).

    LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>).
      MOVE-CORRESPONDING <d> TO result_row.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_system = VALUE #( date    = <d>-credat
                                                                                    time    = <d>-cretim ) ) ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
        GROUP BY (
          tag    = <r>-tag
          mandt  = <r>-mandt
          status = <r>-status
          direct = <r>-direct
          rcvprn = <r>-rcvprn
          sndprn = <r>-sndprn
          mestyp = <r>-mestyp
          idoctp = <r>-idoctp
          value  = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag    = row->tag
          mandt  = row->mandt
          status = row->status
          direct = row->direct
          rcvprn = row->rcvprn
          sndprn = row->sndprn
          mestyp = row->mestyp
          idoctp = row->idoctp
          value  = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp         LIKE result_table.

    SELECT tag,
           mandt,
           status,
           direct,
           rcvprn,
           sndprn,
           mestyp,
           idoctp
      FROM yddg_bd87_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag    = <r>-tag
          mandt  = <r>-mandt
          status = <r>-status
          direct = <r>-direct
          rcvprn = <r>-rcvprn
          sndprn = <r>-sndprn
          mestyp = <r>-mestyp
          idoctp = <r>-idoctp ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.
      IF <r>-direct = '1'.
        metric_out->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).
      ELSE.
        metric_in->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).
      ENDIF.
    ENDLOOP.

    MODIFY yddg_bd87_rb FROM TABLE result_table.
    TRY.
        metric_out->send( ).
        metric_in->send( ).
        DELETE FROM yddg_bd87_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric_out->close( timestamp ).
    metric_in->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
