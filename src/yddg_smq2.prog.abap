REPORT yddg_smq2 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: mandt_dummy TYPE mandt,
      qname_dummy TYPE trfcqnam.

SELECT-OPTIONS: s_mandt FOR mandt_dummy,
                s_qname FOR qname_dummy.

PARAMETERS: p_group TYPE char01 AS CHECKBOX DEFAULT abap_true.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_brack TYPE yddg_bracket DEFAULT 'AGE',
            p_other TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_age:other' LOWER CASE.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'SMQ2'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_smq2( client_range = s_mandt[]
                   queue_range  = s_qname[]
                   grouped      = p_group
                   bracket_set  = p_brack
                   other_tag    = p_other
                   tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
