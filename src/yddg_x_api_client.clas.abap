CLASS yddg_x_api_client DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: host TYPE yddg_cs_value,
          path TYPE string.

    METHODS: constructor IMPORTING !host     TYPE yddg_cs_value
                                   !path     TYPE string
                                   !previous LIKE previous OPTIONAL.

ENDCLASS.



CLASS yddg_x_api_client IMPLEMENTATION.

  METHOD constructor ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '905'
                        attr1    = 'HOST'
                        attr2    = 'PATH' ).

    me->host = host.
    me->path = path.

  ENDMETHOD.

ENDCLASS.
