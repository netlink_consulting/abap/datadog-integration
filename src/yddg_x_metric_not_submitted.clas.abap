CLASS yddg_x_metric_not_submitted DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: count       TYPE i,
          metric      TYPE yddg_metric,
          http_status TYPE i.

    METHODS: constructor IMPORTING !count      TYPE i
                                   !metric      TYPE yddg_metric
                                   !http_status TYPE i
                                   !previous    LIKE previous      OPTIONAL.

ENDCLASS.



CLASS yddg_x_metric_not_submitted IMPLEMENTATION.

  METHOD constructor  ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '906'
                        attr1    = 'COUNT'
                        attr2    = 'METRIC'
                        attr3    = 'HTTP_STATUS' ).

    me->count       = count.
    me->metric      = metric.
    me->http_status = http_status.

  ENDMETHOD.

ENDCLASS.
