REPORT yddg_sm21 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: msgid_dummy  TYPE rslgno,
      mandt_dummy  TYPE mandt,
      user_dummy   TYPE xubname,
      tcode_dummy  TYPE tcode,
      wptype_dummy TYPE rslgwptype,
      prog_dummy   TYPE program_id,
      pack_dummy   TYPE devclass.

SELECT-OPTIONS: s_msgid FOR msgid_dummy,
                s_mandt FOR mandt_dummy,
                s_user FOR user_dummy MATCHCODE OBJECT user_comp,
                s_tcode FOR tcode_dummy,
                s_wptype FOR wptype_dummy,
                s_prog FOR prog_dummy,
                s_pack FOR pack_dummy MATCHCODE OBJECT devclass.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
   ID 'TCD' FIELD 'SM21'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

  AUTHORITY-CHECK OBJECT 'S_ADMI_FCD'
 ID 'S_ADMI_FCD' FIELD 'SM21'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_msgid-low.
  PERFORM f4_msgid IN PROGRAM rsyslog
                   USING 'S_MSGID-LOW'
                         'YNLDDB_SM21'
                         '1000'.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_msgid-high.
  PERFORM f4_msgid IN PROGRAM rsyslog
                   USING 'S_MSGID-HIGH'
                         'YNLDDB_SM21'
                         '1000'.

START-OF-SELECTION.

  NEW yddg_p_sm21( messsageid_range = s_msgid[]
                   client_range     = s_mandt[]
                   user_range       = s_user[]
                   tcode_range      = s_tcode[]
                   wptype_range     = s_wptype[]
                   prog_range       = s_prog[]
                   pack_range       = s_pack[]
                   tag_range        = s_tag[]
                   trace_active     = p_trace )->run( ).
