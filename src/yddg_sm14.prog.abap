REPORT yddg_sm14 MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_TCODE'
                  ID     'TCD' FIELD 'SM14'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.


START-OF-SELECTION.

  NEW yddg_p_sm14( tag_range    = s_tag[]
                   trace_active = p_trace )->run( ).
