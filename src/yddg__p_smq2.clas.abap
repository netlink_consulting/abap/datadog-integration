CLASS yddg__p_smq2 DEFINITION PUBLIC INHERITING FROM yddg__p ABSTRACT CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES: client_range_type TYPE RANGE OF mandt,
           queue_range_type  TYPE RANGE OF trfcqnam.

    CLASS-METHODS: class_constructor.

    METHODS: constructor IMPORTING !client_range TYPE client_range_type OPTIONAL
                                   !queue_range  TYPE queue_range_type OPTIONAL
                                   !grouped      TYPE abap_bool DEFAULT abap_true
                                   !bracket_set  TYPE yddg_bracket DEFAULT 'AGE'
                                   !other_tag    TYPE yddg_tag DEFAULT 'sap_age:other'
                                   !tag_range    TYPE yddg_tag_range_tt OPTIONAL
                                   !trace_active TYPE abap_bool.

  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             mandt     TYPE symandt,
             qname     TYPE trfcqnam,
             dest      TYPE rfcdest,
             qstate    TYPE qrfcstate,
             qrfcuser  TYPE syuname,
             qrfcfnam  TYPE rs38l_fnam,
             qrfcdatum TYPE sydatum,
             qrfcuzeit TYPE syuzeit,
           END OF data_type,

           result_type TYPE yddg_smq2_rb,

           BEGIN OF selector_type,
             client_range TYPE client_range_type,
             queue_range  TYPE queue_range_type,
           END OF selector_type,

           BEGIN OF qgroup_type,
             mandt TYPE mandt,
             qname TYPE RANGE OF trfcqnam,
           END OF qgroup_type.

    CLASS-DATA: qgroup TYPE TABLE OF qgroup_type.

    CLASS-METHODS:

      build_tags  IMPORTING !row        TYPE result_type
                  RETURNING VALUE(tags) TYPE yddg_tag_tt.


    " set in constructor
    DATA: grouped      TYPE abap_bool,
          bracket_set  TYPE yddg_bracket,
          other_tag    TYPE yddg_tag,
          metric       TYPE REF TO yddg__metric,
          selector     TYPE selector_type,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS yddg__p_smq2 IMPLEMENTATION.

  METHOD build_tags.

    APPEND row-tag TO tags.

    APPEND yddg__tags=>resolve( handle = 'CLIENT'          val = row-mandt    ) TO tags.  " sap_client
    APPEND yddg__tags=>resolve( handle = 'DESTINATION'     val = row-dest     ) TO tags.  " sap_destination
    APPEND yddg__tags=>resolve( handle = 'QUEUE_STATUS'    val = row-qstate   ) TO tags.  " sap_queue_status
    APPEND yddg__tags=>resolve( handle = 'USER_ID'         val = row-qrfcuser ) TO tags.  " sap_user_id
    APPEND yddg__tags=>resolve( handle = 'FUNCTION_MODULE' val = row-qrfcfnam ) TO tags.  " sap_function_module
    APPEND yddg__tags=>resolve( handle = 'QUEUE'           val = row-qname    ) TO tags.  " sap_queue

  ENDMETHOD.


  METHOD class_constructor.

    DATA: qname_row TYPE qgroup_type-qname.

    SELECT mandt,
           qname
      FROM qiwktab CLIENT SPECIFIED
      INTO TABLE @DATA(temp).

    LOOP AT temp ASSIGNING FIELD-SYMBOL(<t>).
      IF <t>-qname CA '*'.
        qname_row = VALUE #( ( sign = 'I'  option = 'CP' low = <t>-qname ) ).
      ELSE.
        qname_row = VALUE #( ( sign = 'I'  option = 'EQ' low = <t>-qname ) ).
      ENDIF.
      APPEND VALUE #( mandt = <t>-mandt  qname = qname_row ) TO qgroup.
    ENDLOOP.

  ENDMETHOD.

  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    me->bracket_set       = bracket_set.
    me->other_tag         = other_tag.
    selector-client_range = client_range.
    selector-queue_range  = queue_range.

    metric = get_metric( handle = 'SMQ2'  tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD extract.

    SELECT mandt,
           qname,
           dest,
           qstate,
           qrfcuser,
           qrfcfnam,
           qrfcdatum,
           qrfcuzeit
      FROM trfcqin CLIENT SPECIFIED
      WHERE mandt IN @selector-client_range
        AND qname IN @selector-queue_range
      INTO TABLE @DATA(temp).

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    DATA: result_row TYPE result_type.

    DATA(brackets) = NEW yddg__brackets( bracket_set = bracket_set
                                         timestamp   = timestamp
                                         other_tag   = other_tag ).

    LOOP AT qgroup ASSIGNING FIELD-SYMBOL(<q>).
      LOOP AT data_table ASSIGNING FIELD-SYMBOL(<d>) WHERE mandt = <q>-mandt AND qname IN <q>-qname.
        MOVE-CORRESPONDING <d> TO result_row.
        IF grouped = abap_true.
          result_row-qname = <q>-qname[ 1 ]-low.
        ENDIF.
        result_row-tag = brackets->delta( yddg=>timestamp( datetime_system = VALUE #( date    = <d>-qrfcdatum
                                                                                      time    = <d>-qrfcuzeit ) ) ).
        APPEND result_row TO result_table.
      ENDLOOP.
      DELETE data_table WHERE mandt = <q>-mandt AND qname IN <q>-qname.
    ENDLOOP.
    " capture any that do not fit any entry in SMQR
    LOOP AT data_table ASSIGNING <d>.
      MOVE-CORRESPONDING <d> TO result_row.
      IF grouped = abap_true.
        result_row-qname = space.
      ENDIF.
      result_row-tag = brackets->delta( yddg=>timestamp( datetime_system = VALUE #( date    = <d>-qrfcdatum
                                                                                    time    = <d>-qrfcuzeit ) ) ).
      APPEND result_row TO result_table.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    DATA: temp LIKE result_table.

    LOOP AT result_table ASSIGNING FIELD-SYMBOL(<r>)
      GROUP BY (
          tag      = <r>-tag
          mandt    = <r>-mandt
          qname    = <r>-qname
          dest     = <r>-dest
          qstate   = <r>-qstate
          qrfcuser = <r>-qrfcuser
          qrfcfnam = <r>-qrfcfnam
          value    = GROUP SIZE ) ASCENDING WITHOUT MEMBERS REFERENCE INTO DATA(row).
      APPEND VALUE #(
          tag      = row->tag
          mandt    = row->mandt
          qname    = row->qname
          dest     = row->dest
          qstate   = row->qstate
          qrfcuser = row->qrfcuser
          qrfcfnam = row->qrfcfnam
          value      = row->value ) TO temp.
    ENDLOOP.

    result_table = temp.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA: temp LIKE result_table.

    SELECT tag,
           mandt,
           qname,
           dest,
           qstate,
           qrfcuser,
           qrfcfnam
      FROM yddg_smq2_rb
      INTO CORRESPONDING FIELDS OF TABLE @temp.
    LOOP AT temp ASSIGNING FIELD-SYMBOL(<r>).
      IF NOT line_exists( result_table[
          tag      = <r>-tag
          mandt    = <r>-mandt
          qname    = <r>-qname
          dest     = <r>-dest
          qstate   = <r>-qstate
          qrfcuser = <r>-qrfcuser
          qrfcfnam = <r>-qrfcfnam ] ).
        APPEND <r> TO result_table.
      ENDIF.
    ENDLOOP.

    write_table( name = 'result_table'  tab = result_table ).

    LOOP AT result_table ASSIGNING <r>.

      metric->append( timestamp = timestamp  value = <r>-value  tags = build_tags( <r> ) ).

    ENDLOOP.

    MODIFY yddg_smq2_rb FROM TABLE result_table.
    TRY.
        metric->send( ).
        DELETE FROM yddg_smq2_rb WHERE value = 0.
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.

