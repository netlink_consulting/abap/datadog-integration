CLASS yddg_x_cannot_lock_metric DEFINITION PUBLIC INHERITING FROM yddg_x FINAL CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: user   TYPE xubname,
          metric TYPE yddg_metric.

    METHODS: constructor IMPORTING !user     TYPE xubname
                                   !metric   TYPE yddg_metric
                                   !previous LIKE previous      OPTIONAL.

ENDCLASS.



CLASS yddg_x_cannot_lock_metric IMPLEMENTATION.

  METHOD constructor  ##ADT_SUPPRESS_GENERATION.

    super->constructor( previous = previous
                        msgno    = '907'
                        attr1    = 'USER'
                        attr2    = 'METRIC' ).

    me->user       = user.
    me->metric      = metric.

  ENDMETHOD.

ENDCLASS.
