DEFINE add_line.

  IF &1 IS NOT INITIAL.
    APPEND '|' && &2 && '|' && &1 && '|' TO text.
  ENDIF.

END-OF-DEFINITION.
