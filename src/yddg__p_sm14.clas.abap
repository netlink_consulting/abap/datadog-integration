class YDDG__P_SM14 definition
  public
  inheriting from YDDG__P
  abstract
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !TAG_RANGE type YDDG_TAG_RANGE_TT optional
      !TRACE_ACTIVE type ABAP_BOOL .
  PROTECTED SECTION.

    TYPES: BEGIN OF data_type,
             status TYPE vbinfo-vb_active,
           END OF data_type,

           BEGIN OF result_type,
             value TYPE typint8,
           END OF result_type.

    " set in constructor
    DATA: metric       TYPE REF TO yddg__metric,

          " set in extract
          data_table   TYPE STANDARD TABLE OF data_type,

          " set in transform
          result_table TYPE TABLE OF result_type.

    METHODS:

      extract   REDEFINITION,
      transform REDEFINITION,
      load      REDEFINITION,
      finalize  REDEFINITION.

ENDCLASS.



CLASS YDDG__P_SM14 IMPLEMENTATION.


  METHOD constructor.

    super->constructor( tag_range = tag_range  trace_active = trace_active ).

    metric = get_metric( handle = 'SM14'  tags  = tags  trace_active = trace_active ).

  ENDMETHOD.


  METHOD extract.

    CONSTANTS: vb_get_info TYPE x LENGTH 1 VALUE 17.

    DATA: info TYPE vbinfo.

    CALL 'ThVBCall'
      ID 'OPCODE' FIELD vb_get_info
      ID 'VBINFO' FIELD info.

    APPEND VALUE #( status = info-vb_active ) TO data_table.

    write_table( name = 'data_table'  tab = data_table ).

  ENDMETHOD.


  METHOD transform.

    IF data_table[ 1 ]-status = 1.
      APPEND VALUE #( value = 1 ) TO result_table.
    ELSE.
      APPEND VALUE #( value = 2 ) TO result_table.
    ENDIF.

    write_table( name = 'result_table'  tab = result_table ).

  ENDMETHOD.


  METHOD load.

    DATA(data_row) = data_table[ 1 ].

    metric->append( timestamp = timestamp  value = result_table[ 1 ]-value ).

    TRY.
        metric->send( ).
      CATCH yddg_x_metric_not_submitted INTO DATA(cx).
        MESSAGE cx TYPE yddg=>submit_error_severity.
    ENDTRY.

  ENDMETHOD.


  METHOD finalize.

    metric->close( timestamp ).

    COMMIT WORK.

  ENDMETHOD.

ENDCLASS.
