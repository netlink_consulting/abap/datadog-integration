REPORT yddg_sost MESSAGE-ID yddg
                 NO STANDARD PAGE HEADING
                 LINE-SIZE 1023.

DATA: user_dummy    TYPE xubname,
      snd_art_dummy TYPE sx_addrtyp.

SELECT-OPTIONS: s_user FOR user_dummy MATCHCODE OBJECT user_comp.

SELECTION-SCREEN SKIP 1.

SELECT-OPTIONS: s_sndart FOR snd_art_dummy.

SELECTION-SCREEN SKIP 1.

SELECTION-SCREEN BEGIN OF BLOCK bl1 WITH FRAME TITLE TEXT-001.

PARAMETERS: p_maxcur TYPE yddg_seconds DEFAULT 86400.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_bracur TYPE yddg_bracket DEFAULT 'AGE',
            p_othcur TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_age:other' LOWER CASE.

SELECTION-SCREEN END OF BLOCK bl1.

SELECTION-SCREEN BEGIN OF BLOCK bl2 WITH FRAME TITLE TEXT-002.

PARAMETERS: p_maxerr TYPE yddg_seconds DEFAULT 31536000.

SELECTION-SCREEN SKIP 1.

PARAMETERS: p_braerr TYPE yddg_bracket DEFAULT 'AGE',
            p_otherr TYPE yddg_tag VISIBLE LENGTH 25 DEFAULT 'sap_age:other' LOWER CASE.

SELECTION-SCREEN END OF BLOCK bl2.

SELECTION-SCREEN SKIP 1.

INCLUDE yddg_common_parameters.

INITIALIZATION.

  AUTHORITY-CHECK OBJECT 'S_OC_ROLE'
                   ID     'OFFADMI'
                   FIELD  'ADMINISTRATOR'.
  IF sy-subrc <> 0.
    MESSAGE e003 WITH sy-repid.
  ENDIF.

START-OF-SELECTION.

  NEW yddg_p_sost( user_range          = s_user[]
                   send_type_range     = s_sndart[]
                   max_age_current     = p_maxcur
                   bracket_set_current = p_bracur
                   other_tag_current   = p_othcur
                   max_age_error       = p_maxerr
                   bracket_set_error   = p_braerr
                   other_tag_error     = p_otherr
                   tag_range           = s_tag[]
                   trace_active        = p_trace )->run( ).
