
# Datadog Integration for SAP

# [- Warning: This is a preview. -]

## Send your Datadog Metrics directly from your SAP system

### Requirements:

- [ ] Datadog account (check [www.datadoghq.com](https://www.datadoghq.com/) for a free trial)
- [ ] recent SAP ABAP system version (750 should work)

## Installation 

Coming versions will be available as SAP Transports.

For this preview, use [abapGit](https://abapgit.org/) and clone to package `YDDG`.

## Setup 

1. Any communication uses `https`, therefore the respective certificates must exist in `STRUST` _SSL client SSL Client (Standard)_:
    - `CN=DigiCert Global Root CA, OU=www.digicert.com, O=DigiCert Inc, C=US`
    - `CN=DigiCert TLS RSA SHA256 2020 CA1, O=DigiCert Inc, C=US`

   They can be obtained from the Datadog website.

2. Provide your configuration using `YDDG_INIT`:

   | Item                         | Description                                                              |
   | ---------------------------- | ------------------------------------------------------------------------ |
   | Datadog API Key              | obtain from Datadog site                                                 |
   | Datadog API Host             | usually `api.datadoghq.com` might be different outside U.S.              |
   | Datadog Log Intake Host      | usually `http-intake.logs.datadoghq.com` might be different outside U.S. |
   | Reserved Tag: env            | e.g. `DEV`, `QAS`, or `PRD`                                              |
   | Hostname reported to Datadog | recommendation: host running the **ASCS**                                |

3. Add content in control tables using `YDDG_CONFIG` (see below for content)
    - Global Tags: no entry required, any entry will be submitted with every metrics 
    - Tags
    - Metrics 
    - Bracket Sets
        - Thresholds


#### Tables 

##### Tags

| Handle          | Tag                                 |
| --------------- | ----------------------------------- |
| ASHOST          | sap_instance                        |
| CLIENT          | sap_client                          |
| CLIENT_ROLE     | sap_client_role                     |
| CUAPROGRAM      | sap_cuaprogram                      |
| DESTINATION     | sap_destination                     |
| EVENT_ID        | sap_event_id                        |
| FUNCTION_MODULE | sap_function_module                 |
| IS_PERIODIC     | sap_job_is_periodic                 |
| JOB_CLASS       | sap_job_class                       |
| JOB_NAME        | sap_job_name                        |
| LOCK_MODE       | sap_lock_mode                       |
| LOCK_OBJECT     | sap_lock_object                     |
| QUEUE           | sap_queue                           |
| QUEUE_STATUS    | sap_queue_status                    |
| REPORT          | sap_report                          |
| SM12_E          | exclusive                           |
| SM12_O          | optimistic                          |
| SM12_S          | shared                              |
| SM12_X          | exclusive_non-cumulative            |
| SM13_0          | processed                           |
| SM13_1          | processed_v1                        |
| SM13_2          | started                             |
| SM13_20         | stopped_(no_retry)                  |
| SM13_21         | error_in_v2_part                    |
| SM13_22         | error_in_collection_run             |
| SM13_23         | to_delete                           |
| SM13_3          | prepared                            |
| SM13_4          | processed_v2                        |
| SM13_40         | canceled                            |
| SM13_41         | enqueues_deleted                    |
| SM13_42         | error                               |
| SM13_43         | error_(external_data/communication) |
| SM13_44         | error_(no_retry)                    |
| SM13_5          | auto_(dia)                          |
| SM13_6          | auto_(sys)                          |
| SM13_7          | initial                             |
| STATUS_ERROR    | status:error                        |
| STATUS_INFO     | status:info                         |
| STATUS_WARN     | status:warn                         |
| TABNAME         | sap_table                           |
| TCODE           | sap_tcode                           |
| UPDATER_STATUS  | sap_updater_status                  |
| USER_ID         | sap_user_id                         |
| WP_HOLD         | sap_wp_hold                         |
| WP_HOLD_INFO    | sap_wp_hold_info                    |
| WP_STATUS       | sap_wp_status                       |
| WP_TYPE         | sap_wp_type                         |

##### Metrics 

| Handle       | Int. | Datadog Metric                   |
| ------------ | ---- | -------------------------------- |
| SCC4_CC      | 3    | sap.abap.scc4.cross_client       |
| SCC4_CP      | 3    | sap.abap.scc4.copy_protection    |
| SCC4_CR      | 3    | sap.abap.scc4.catt_restriction   |
| SCC4_CS      | 3    | sap.abap.scc4.client_specific    |
| SCC4_UP      | 3    | sap.abap.scc4.upgrade_protection |
| SM12         | 3    | sap.abap.sm12                    |
| SM13         | 3    | sap.abap.sm13                    |
| SM14         | 3    | sap.abap.sm14                    |
| SM37_A       | 3    | sap.abap.sm37.canceled           |
| SM37_F       | 3    | sap.abap.sm37.finished           |
| SM37_R       | 3    | sap.abap.sm37.active             |
| SM66         | 3    | sap.abap.sm66                    |
| SMQ1         | 3    | sap.abap.smq1                    |
| SMQ2         | 3    | sap.abap.smq2                    |
| SMQS_CURRENT | 3    | sap.abap.smqs.current            |
| SMQS_MAX     | 3    | sap.abap.smqs.max                |


##### Bracket Sets

|Bracket Set|Description|
| --- | --- |
|AGE	|Default Age Brackets|
|DURATION|	Default Duration Brackets|

##### Thresholds (Bracket Set: AGE)

|Seconds| Tag |
|---:|---|
|     300|sap_age:d000h00m05    |
|     900|sap_age:d000h00m15    |
|    1800|sap_age:d000h00m30    |
|    3600|sap_age:d000h01       |
|    7200|sap_age:d000h02       |
|   14400|sap_age:d000h04       |
|   28800|sap_age:d000h08       |
|   43200|sap_age:d000h12       |
|   86400|sap_age:d001          |
|  172800|sap_age:d002          |
|  604800|sap_age:d007          |
| 2419200|sap_age:d028          |
|15811200|sap_age:d183          |
|31536000|sap_age:d365          |

##### Thresholds (Bracket Set: DURATION)

|Seconds| Tag |
|---:|---|
|    1|sap_duration:h00m00s01  |
|    2|sap_duration:h00m00s02  |
|    5|sap_duration:h00m00s05  |
|   10|sap_duration:h00m00s10  |
|   15|sap_duration:h00m00s15  |
|   30|sap_duration:h00m00s30  |
|   60|sap_duration:h00m01     |
|  120|sap_duration:h00m02     |
|  300|sap_duration:h00m05     |
|  900|sap_duration:h00m15     |
| 1800|sap_duration:h00m30     |
| 3600|sap_duration:h01        |
| 7200|sap_duration:h02        |
|14400|sap_duration:h04        |
|28800|sap_duration:h08        |
|43200|sap_duration:h12        |
|57600|sap_duration:h16        |
|86400|sap_duration:h24        |


## Usage

The metrics a generated and send by calling (scheduling) ABAP reports. While the reports provide options to include or exclude certain information, the defaults should be sufficient to start. To get continuous monitoring schedule the reports to run periodically (e.g. every minute) as Background Jobs using `SM36`.

The report names reflect the corresponding SAP Transactions.

### YDDG_SCC4

**Current status of client Change Restrictions and Protections.**

Five metrics with the lowest value being `1`. The higher the value the more _open_ the client.

This can be used to monitor that a production client cannot be changed.


### YDDG_SM12

**Current locks** differentiated (using Tags) by:
- Age
- Client 
- User 
- Table 
- Lock mode
- Lock Object
- Transaction Code

### YDDG_SM13

**Update requests** differentiated (using Tags) by:
- Age
- Client 
- User 
- Status 
- Transaction Code

### YDDG_SM14

- **Updater active**: `1`
- **Updater not active**: `2`

Can be used to alert when Updater is not active.

### YDDG_SM37

**Backgroud Jobs** (active/finish/canceled) differentiated (using Tags) by:
- Age
- Client 
- User 
- Job name 
- Job class
- Instance
- Periodic 
- Event id 

### YDDG_SM66

**Workprocesses** differentiated (using Tags) by:
- Instance
- Type
- Status 
- Hold Information 
- Client 
- User 
- Transaction Code
- Report 
- CUA program

### YDDG_SMQ1

**Outbound tRFC** differentiated (using Tags) by:
- Client 
- Destination 
- Queue 

### YDDG_SMQ2

**Inbound tRFC** differentiated (using Tags) by:
- Client 
- Destination 
- Queue 
- Queue status 
- User 
- Function module 

The value for _Queue_ is grouped using the information from `SMQR` by default.

### YDDG_SMQS

**Outbound tRFC** (max and current) differentiated (using Tags) by:
- Client 
- Destination 

# Remark

As mentioned above, this is a preview everything has been tested on multiple systems, but YMMV.

Please contact <datadog@netlink-consulting.com> for questions, ideas, feedback, recommendations...
